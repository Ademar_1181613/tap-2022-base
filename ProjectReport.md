# **Relatório de projeto**

MEI
TAP
2021/2022

Realizado por:

- Miguel Silva, 1181076
- Ademar Vale, 1181613
- Ivo Curado, 1180935

## **Introdução**

Considerando as necessidades de escalonamento de uma série de enfermeiras num estabelecimento hospitalar, este projeto visa cobrir três etapas (Milestones) de desenvolvimento de um algoritmo em Scala, capaz de receber uma série de dados de input, no formato de ficheiros XML, elaborando o seu consequente processamento, e tomando os resultados (horários em que existe a associação de enfermeiras a turnos, ao longo de uma série de periodos, durante uma semana), exportando-os também para ficheiros XML.

Assim sendo, este documento têm como objetivo a descrição e análise de todo o projeto realizado, incluindo a **Milestone 1**, a **Milestone 2** e a **Milestone 3**, sendo em cada secção descritos os aspetos de design, implementação e testagem.

## **Contextualização do Projeto**

Para desenvolver as várias etapas do algoritmo, primeiro é preciso ter uma noção do negócio e dos conceitos e dinâmicas que o constituem.

O algoritmo irá receber várias informações para processar, incluindo:

- **Enfermeiras**: Possuem um valor de senioridade (seniority) e um ou mais papéis (roles) que podem executar no hospital. Terão de ser associadas a Shifts pelo algoritmo.
- **Periodos**: Representam os vários blocos ao longo de um dia que deverão ser preenchidos por Shifts. Cada período terá portanto de corresponder a um shift no output. Um período lista sempre o seu tempo de início e fim e uma lista de requerimentos (requirements) que têm de ser tidos em consideração ao escolher as nurses que vão trabalhar nele.
- **Requerimentos**: Indicam a necessidade de um número específico de enfermeiras com um certo papel. Por exemplo, 2 enfermeiras com o papel A.
- **Constraints**: Devem existir duas, uma denominada MaxShiftsPerDay, que refere o número máximo de shifts aos quais uma enfermeira pode estar associada num único dia; e MinRestDaysPerWeek, que indica o número mínimo de dias que uma enfermeira tem de ter de descanso, sem nenhum shift (ou seja, dias livres)
- **Preferências**: Ditam as preferências que enfermeiras têm perante um dia ou período específico. Possuem um valor númerico entre -2 e 2 que deve ser associado à senioridade da enfermeira para determinar o valor de preferência final.

Relativamente aos outputs esperados, existem os seguintes conceitos:

- **Shift Schedule**: Horário resultante, dividido em 7 dias, cada dia com uma série de turnos (shifts);
- **Shift Days**: Dias que constituem o horário e que apresentam vários turnos. Têm uma identificação de 1 a 7.
- **Shift**: Turno que corresponde a um periodo de input, com uma lista de nurses, cada uma com um papel único atribuido.

## **Milestone 01**

A primeira parte do projeto, denominada **Milestone**, tem como objetivo definir as várias entidades do negócio e o desenvolvimento de uma versão inicial do algoritmo desejado.

Com isto, deseja-se obter um produto, quanto à geração de um horário com o escalonamento de enfermeiras, de um hospital, minimamente viável. Para tal, apenas serão consideradas as constraints associadas ao número máximo de shifts num único dia, sendo ignoradas, para já, as constraints de mínimo de dias de descanso e a utilização de preferências na escolha de nurses.

Dessa forma desenhou-se o modelo de dominio para o problema de escalonamento de enfermeiras em questão e, de seguida, implementou-se um algoritmo que realiza o escalonamento, com algumas restrições.

### **Modelo de domínio**

O modelo de domínio desenhado é o seguinte:

![Modelo de dominio](./src/diagrams/domain_model.png)

Do lado esquerdo, encontram-se as informações de input listadas numa secção anterior. Do lado direito, apresentam-se as entidades que vão constituir o output gerado.

### **Opaque Types**

Neste projeto foram utilizados **opaque types** com o objetivo de não utilizar diretamente tipos primitivos e, desta forma, aplicar o conceito de **Single-responsability principle**, onde cada **opaque type** é responsável pela sua própria validação.

Para esse mesmo efeito, de forma a definirem-se os possíveis erros e podermos tratar os mesmos, foram criados os seguintes **Domain Errors**:


```

enum DomainError:
  //XML
  case IOFileProblem(error: String)
  case XMLError(error: String)
  //Nurse
  case NotEnoughNurses
  case EmptyName
  case EmptyIdError(error: String)
  case NoShiftsError(error: String)
  case NoNursesError(error: String)
  case MissingDaysError(error: String)
  case SeniorityValueOutOfRange(number: Int)
  case NoRolesError(error: String)
  case InvalidRole(error: String)
  //SchedulingPeriod
  case InvalidPeriod(error: String)
  case NoNurseRequirementsError(error: String)
  case InvalidNumber(error: String)
  //Constraint
  case InvalidMinRestDays(error: String)
  case InvalidMaxShiftsPerDay(error: String)
  //Day and Period
  case PreferenceValueOutOfRange(number: Int)
  case InvalidValue(error: String)
  case InvalidDayValue(error: String)
  //Scheduling
  case InsufficientServiceNecessities(error: String)
  case UnknownRequirementRole(role: String)

```


Desta forma, as **case classes** não têm a responsabilidade de validar conceitos de domínio.

Foram utilizados os seguintes **opaque types**:


```

object Simple :
  opaque type StringId = String
  object StringId:
    def from(id: String): Result[StringId] =
      if(id.trim.nonEmpty)
        Right(id)
      else
        Left(DomainError.EmptyIdError("Invalid ID (empty)"))
    def toString(stringId: StringId): String =
      stringId


  opaque type Role = String
  object Role:
    def from(role: String): Result[Role] =
      if(role.trim.nonEmpty)
        Right(role)
      else
        Left(DomainError.InvalidRole("Invalid role (empty) for Nurse"))
    def toString(role: Role):String =
      role

  opaque type DayNumber = Int
  object DayNumber:
    def from(number: Int): Result[DayNumber] =
      if(number>0 && number<8)
          Right(number)
      else
          Left(DomainError.InvalidDayValue("Day Number is not valid (not in the 1-7 interval)"))
    def toString(day: DayNumber): String =
      day.toString

  opaque type NurseName = String
  object NurseName:
    def from(name: String): Result[NurseName] =
      if(name.trim.nonEmpty)
        Right(name)
      else
        Left(DomainError.EmptyName)
    def toString(name: NurseName): String =
      name.toString
  
  opaque type NurseSeniority = Int
  object NurseSeniority:
    def from(seniority: Int): Result[NurseSeniority] =
      if(seniority>=1 && seniority<=5)
        Right(seniority)
      else
        Left(DomainError.SeniorityValueOutOfRange(seniority))

  opaque type PreferenceValue = Int
  object PreferenceValue:
    def from(number: Int): Result[PreferenceValue]=
      if(number >= -2 && number <= 2)
        Right(number)
      else
        Left(DomainError.PreferenceValueOutOfRange(number))

  opaque type MaxShiftsPerDay = Int
  object MaxShiftsPerDay:
    def from(number: Int): Result[MaxShiftsPerDay]=
      if(number > 0)
        Right(number)
      else
        Left(DomainError.InvalidMaxShiftsPerDay("Max shifts per day should be a positive number!"))

    def compareMaxShiftsPerDay(count: Int, maxShiftsPerDay: MaxShiftsPerDay): Boolean =
        if(count == maxShiftsPerDay) false else true


  opaque type MinRestDaysPerWeek = Int
  object MinRestDaysPerWeek:
    def from(number: Int): Result[MinRestDaysPerWeek] =
      if(number >= 0 && number < 8)
        Right(number)
      else
        Left(DomainError.InvalidMinRestDays("Min Rest Days Per Week should be between 0 and 7"))

```


Uma alternativa a esta abordagem, apesar de a nosso ver menos correcta, seria utilizar diretamente os tipos primitivos e validar os conceitos de domínio no construtor da classe em questão.

### **Case Classes**

De forma a representar os elementos apresentados no **Modelo de domínio** foram criadas **case classes**.
Cada uma das classes possui um método **from** responsável por transformar um **Xml Node** em um objeto do respétivo domínio.
A seguir é apresentado o código referente a uma case classe, a Nurse, em que é demonstrado o método **from**, previamente referido.


```

case class Nurse private(name: NurseName, seniority: NurseSeniority, roles: Set[Role])

object Nurse:

  def from(xml: Node): Result[Nurse] =
    for{
      name <- fromAttribute(xml, "name")
      nurseName <- NurseName.from(name)
      seniority <- fromAttribute(xml, "seniority")
      nurseSeniority <- NurseSeniority.from(seniority.toInt)
      roles <- traverse((xml \\ "nurseRole"), fromRole)
    } yield Nurse(nurseName, nurseSeniority, roles.toSet)

  def from(name: NurseName, seniority: NurseSeniority, roles: Set[Role]): Result[Nurse] =
    if (roles.size > 0)
      Right(Nurse(name, seniority, roles))
    else
      Left(DomainError.NoRolesError("The nurse must have at least 1 role"))


  def fromRole(xml: Node): Result[Role] =
    for {
      r <- fromAttribute(xml, "role")
      role <-Role.from(r)
    } yield role

```


Com o objetivo de poder exportar para um ficheiro XML o resultado do **Scheduling Algorithm**,
várias classes implementam também um método **toXML**.

Essas classes são:

- **ShiftNurse**
- **Shift**
- **ShiftDay**
- **ShiftSchedule**

Este método têm como objetivo a conversão de objetos de domíno em elementos **XML**. Dessa forma podemos exportar para ficheiros **XML** o resultado final do **Scheduling Algorithm**.
De seguida, é apresentado um exemplo de um método **toXML** implementado. O método referente à conversão de um objeto **Shift** em um elemento **XML** que represente o mesmo.


```

  def toXML(shift: Shift): Elem =
    <shift id={StringId.toString(shift.id)}>
      { shift.nurses.map( n => ShiftNurse.toXML(n)) }
    </shift>

```


### **Algoritmo**

O algoritmo implementado para a 1ª **Milestone** tem em consideração os seguintes requisitos:

- Implementar as restrições referentes a um único dia.
- O algoritmo deve apenas resolver o escalonamento para um dia e os restantes dias podem ser replicados a partir desse dia gerado.
- O algoritmo deve ser simples e escolher a primeira nurse que verificar as restrições a serem consideradas.

De uma vista geral, o algoritmo consiste em ler ficheiros XML, criar um _ScheduleInfo_ com todas as informações de input e gerar um _ShiftSchedule_, isto é, o horário com o escalonamento das enfermeiras.

Apresentando o algoritmo de uma forma mais detalhada, o algoritmo possui as seguintes etapas:

1. Percorrer todos os **SchedulingPeriod's**.
2. Obter o **Shift** correspondente a cada **SchedulingPeriod**, utilizando o método **createShifts**.
3. Validar todas as constraints durante todo o processo utilizando a estrutura **if** e o método **checkForUnknownRoles**.
4. Criar um **ShifSchedule** para 7 dias, utilizando os Shifts obtidos com a etapa 2.


Para a criação de um ficheiro **output** com o **ShiftSchedule** elaborado, foi implementado um método **create** responsável por ler um ficheiro de **input** **XML**, percorrer toda a informação no mesmo e aplicar o algoritmo desenvolvido.
De seguida, é possivel verificar esse método **create** implementado.


```

  def create(xml: Elem): Result[Elem] =
    for {
      si <- ScheduleInfo.from(xml)
      shiftSchedule <- getShiftSchedule(si)
    } yield ShiftSchedule.toXML(shiftSchedule)

```


A lógica to algoritmo está implementada na classe **SchedulingAlgorithmUtils01**.
Nesta classe temos o algoritmo principal presente no método **getShiftSchedule**, método este chamado no método **create**, previamente apresentado.


```

  def getShiftSchedule(si: ScheduleInfo): Result[ShiftSchedule] =
    val difference = checkForUnknownRoles(si)
    if(difference.size!=0)
      Left(DomainError.UnknownRequirementRole(difference.head))
    else
      val s: Set[Shift] = si.schedulingPeriods.foldLeft[Set[Shift]](Set()){
        (acc, sp) =>
          createShifts(acc, si.constraints, sp.id, si.nurses, sp.nurseRequirement) match
            case Left(value) => acc
            case Right(value) => acc ++ Set(value)
      }
      if(s.size<si.schedulingPeriods.size)
        Left(DomainError.NotEnoughNurses)
      else
        val sets =
          for{
            d1 <- DayNumber.from(1)
            sd1 <- ShiftDay.from(d1, s)
            d2 <- DayNumber.from(2)
            sd2 <- ShiftDay.from(d2, s)
            d3 <- DayNumber.from(3)
            sd3 <- ShiftDay.from(d3, s)
            d4 <- DayNumber.from(4)
            sd4 <- ShiftDay.from(d4, s)
            d5 <- DayNumber.from(5)
            sd5 <- ShiftDay.from(d5, s)
            d6 <- DayNumber.from(6)
            sd6 <- ShiftDay.from(d6, s)
            d7 <- DayNumber.from(7)
            sd7 <- ShiftDay.from(d7, s)
          } yield  ShiftSchedule(Set(sd1,sd2,sd3,sd4,sd5,sd6,sd7))
        sets

```


Por sua vez, o método **createShifts**, chamado na etapa dois, encontra-se no seguinte excerto de código:


```


  def createShifts(shifts: Set[Shift], constraints: Constraint, id: StringId, nurses: Set[Nurse], nr: Set[NurseRequirement]): Result[Shift] =
    val sn: Set[ShiftNurse] = nr.foldLeft[Set[ShiftNurse]](Set()){
      (acc, req) =>
        val filteredNurses = nurses.filter(n => n.roles.exists(role => role.toString == req.role.toString) )
        val tmp = filteredNurses.foldLeft[Set[ShiftNurse]](Set()){
          (nurses, nur) =>
            val nameString = nur.name.toString
            if(maxShiftsPerDayValidation(constraints, shifts, nameString) && !nurseAlreadyInShiftValidation(acc, nameString) && !nurseRequirementMet(nurses, req.role, req.number))
              val res =
                for{
                  nn <- ShiftNurse.from(nameString, req.role)
                } yield nn
              res match
                case Left(value) => nurses
                case Right(value) => nurses ++ Set(value)
            else
              nurses
        }
        acc ++ tmp
    }
    val sumNumber = nr.foldLeft[Int](0){(sum, rr) => sum + rr.number}
    Shift.from(id,sn, sumNumber)

```


O método **createShifts** consiste em:

1. Percorrer os **NurseRequirement's**
2. Para cada **NurseRequirement** obter as possíveis **Nurse's** a serem atribuitas ao **NurseRequirement**.
3. Percorrer as possiveis **Nurse's**
4. Para cada **Nurse**, se todas as validações tiverem sucesso, irá-se concatenar um Set[**NurseShift**] acumulativo com um novo Set, sendo que este possui uma nova **NurseShift**, sendo constituida pelo nome da **Nurse**, role do **NuresRequirement**
5. Obtendo um Set[NurseShift] que satisfaça as validações, irá se criar  um Shift como o conjunto de **NurseShift's** obtido na etapa anterior

### **Testes**

Foram desenvolvidas classes de teste de forma a validar todos os **opaque types**, **case classes** e uma classe extra para validar os métodos existentes na **SchedulingAlgorithmUtils**.


- **Exemplo de testes aos opaque types**


```

class  SimpleTest extends AnyFunSuite:

  test("String id is empty"){
    val result = StringId.from("")
    assert(result.fold(error => error == DomainError.EmptyIdError("Invalid ID (empty)"), _ => false))
  }

  test("String id is blank"){
    val result = StringId.from("        ")
    assert(result.fold(error => error == DomainError.EmptyIdError("Invalid ID (empty)"), _ => false))
  }

  test("String id is created"){
    val result = StringId.from("identification")
    assert(result.fold(_ => false, _ => true))
  }

  test("Role is empty"){
    val result = Role.from("")
    assert(result.fold(error => error == DomainError.InvalidRole("Invalid role (empty) for Nurse"), _ => false))
  }

  test("Role is blank"){
    val result = Role.from("        ")
    assert(result.fold(error => error == DomainError.InvalidRole("Invalid role (empty) for Nurse"), _ => false))
  }

  test("Role is created"){
    val result = Role.from("role")
    assert(result.fold(_ => false, _ => true))
  }

  test("Day number is below 1"){
    val result = DayNumber.from(0)
    assert(result.fold(error => error == DomainError.InvalidDayValue("Day Number is not valid (not in the 1-7 interval)"), _ => false))
  }

  test("Day number is above 7"){
    val result = DayNumber.from(8)
    assert(result.fold(error => error == DomainError.InvalidDayValue("Day Number is not valid (not in the 1-7 interval)"), _ => false))
  }

  test("Day number is created"){
    val result = DayNumber.from(4)
    assert(result.fold(_ => false, _ => true))
  }

  test("NurseName is empty"){
    val result = NurseName.from("")
    assert(result.fold(error => error == DomainError.EmptyName, _ => false))
  }

  test("NurseName is blank"){
    val result = NurseName.from("        ")
    assert(result.fold(error => error == DomainError.EmptyName, _ => false))
  }

  test("NurseName is created"){
    val result = NurseName.from("Ana")
    assert(result.fold(_ => false, _ => true))
  }

  test("NurseSeniority is below 1"){
    val result = NurseSeniority.from(0)
    assert(result.fold(error => error == DomainError.SeniorityValueOutOfRange(0), _ => false))
  }

  test("NurseSeniority is above 5"){
    val result = NurseSeniority.from(6)
    assert(result.fold(error => error == DomainError.SeniorityValueOutOfRange(6), _ => false))
  }

  test("NurseSeniority is created"){
    val result = NurseSeniority.from(3)
    assert(result.fold(_ => false, _ => true))
  }

  test("PreferenceValue is created"){
    val result = PreferenceValue.from(2)
    assert(result.fold(_ => false, _ => true))
  }

  test("PreferenceValue is above 2"){
    val result = PreferenceValue.from(3)
    assert(result.fold(error => error == DomainError.PreferenceValueOutOfRange(3), _ => false))
  }

  test("PreferenceValue is below -2"){
    val result = PreferenceValue.from(-3)
    assert(result.fold(error => error == DomainError.PreferenceValueOutOfRange(-3), _ => false))
  }

  test("MaxShiftsPerDay is created"){
    val result = MaxShiftsPerDay.from(1)
    assert(result.fold(_ => false, _ => true))
  }

  test("MaxShiftsPerDay is below 0"){
    val result = MaxShiftsPerDay.from(-1)
    assert(result.fold(error => error == DomainError.InvalidMaxShiftsPerDay("Max shifts per day should be a positive number!"), _ => false))
  }

  test("MinRestDaysPerWeek is below 0"){
    val result = MinRestDaysPerWeek.from(-1)
    assert(result.fold(error => error == DomainError.InvalidMinRestDays("Min Rest Days Per Week should be between 0 and 7"), _ => false))
  }

  test("MinRestDaysPerWeek is above 7"){
    val result = MinRestDaysPerWeek.from(8)
    assert(result.fold(error => error == DomainError.InvalidMinRestDays("Min Rest Days Per Week should be between 0 and 7"), _ => false))
  }

  test("MinRestDaysPerWeek is created"){
    val result = MinRestDaysPerWeek.from(1)
    assert(result.fold(_ => false, _ => true))
  }

```


*   **Exemplo de testes às case classes**


```

class NurseTest extends AnyFunSuite:
  
  test("Nurse Seniority without roles"){
    val result = 
      for{
        nurseName <- NurseName.from("Ana")
        nurseSeniority <- NurseSeniority.from(3)
        roleA <- Role.from("A")
        roleB <- Role.from("B")
      } yield Nurse.from(nurseName, nurseSeniority,Set[Role]())
    assert(result.fold(error => error == DomainError.NoRolesError("The nurse must have at least 1 role"), _ => true))
  }

  test("Nurse is created"){
    val result = 
      for{
        nurseName <- NurseName.from("Ana")
        nurseSeniority <- NurseSeniority.from(3)
        roleA <- Role.from("A")
        roleB <- Role.from("B")
      } yield Nurse.from(nurseName, nurseSeniority,Set(roleA, roleB))
    assert(result.fold(_ => false, _ => true))
  }


```


*   **Exemplo de testes à classe SchedulingAlgorithmUtils**


```

class SchedulingAlgorithmUtils01Test extends AnyFunSuite:

  test("Max Shifts Per day reached"){
    val res =
      for {
        role <- Role.from("role")
        id1 <- StringId.from("morning")
        id2 <- StringId.from("afternoon")
        id3 <- StringId.from("evening")
        m1 <- MinRestDaysPerWeek.from(2)
        m2 <- MaxShiftsPerDay.from(3)
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
        shift1 <- Shift.from(id1, Set(nurse1, nurse2), 2)
        shift2 <- Shift.from(id2, Set(nurse1, nurse2), 2)
        shift3 <- Shift.from(id3, Set(nurse1, nurse2), 2)
      }
      yield SchedulingAlgorithmUtils01.maxShiftsPerDayValidation(Constraint.from(m1,m2),  Set(shift1,shift2,shift3), "Maria")
    assert(res.fold(_=>false, _=> res) == Right(false))
  }

  test("Max Shifts Per day not reached"){
    val res = 
      for {
        role <- Role.from("role")
        id1 <- StringId.from("morning")
        id2 <- StringId.from("afternoon")
        id3 <- StringId.from("evening")
        m1 <- MinRestDaysPerWeek.from(2)
        m2 <- MaxShiftsPerDay.from(3)
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
        nurse3 <- ShiftNurse.from("Joana", role)
        shift1 <- Shift.from(id1, Set(nurse1, nurse2), 2)
        shift2 <- Shift.from(id2, Set(nurse1, nurse2), 2)
        shift3 <- Shift.from(id3, Set(nurse1, nurse3), 2)
      }
      yield SchedulingAlgorithmUtils01.maxShiftsPerDayValidation(Constraint(m1,m2),  Set(shift1,shift2,shift3), "Maria")
    assert(res.fold(_=>false, _=> res) == Right(true))
  }

  test("Shift: nurse not yet in it"){
    val res = 
      for {        
        role <- Role.from("role")
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
      }
      yield SchedulingAlgorithmUtils01.nurseAlreadyInShiftValidation(Set(nurse1, nurse2), "Joana")
    assert(res.fold(_=>false, _=> res) == Right(false))
  }
  
  test("Shift: nurse is already in it"){
    val res = 
      for {
        role <- Role.from("role")
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
      }
      yield SchedulingAlgorithmUtils01.nurseAlreadyInShiftValidation(Set(nurse1, nurse2), "Ana")
    assert(res.fold(_=>false, _=> res) == Right(true))
  }


  test("Shift number of nurses is not met"){
    val res = 
      for {
        role <- Role.from("role")
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
      }
      yield SchedulingAlgorithmUtils01.nurseRequirementMet(Set(nurse1, nurse2), role, 4)
    assert(res.fold(_=>false, _=> res) == Right(false))
  }
  
  test("Shift number of nurses is met"){
    val res = 
      for {
        role <- Role.from("role")
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
      }
      yield SchedulingAlgorithmUtils01.nurseRequirementMet(Set(nurse1, nurse2), role, 2)
    assert(res.fold(_=>false, _=> res) == Right(true))
  }
  


```


Para testes de **Integração** do algoritmo foram utilizados os testes fornecidos no projeto base, que incluiam um grupo de ficheiros de input XML e respetivos ficheiro output XML ao qual o output gerado pelo algoritmo seria comparado.

Quanto aos 20 testes de integração fornecidos nesta Milestone, todos os testes passam com sucesso:

![Testes fornecidos executados para a Milestone 1](./src/images/ms01tests.png)

Ainda mais, foram desenvolvidos testes de integração adicionais pelo grupo usando esta metodologia. Desenvolveram-se 10 testes de integração diferentes dos fornecidos, estando todos a passar com sucesso:

![Testes custom executados para a Milestone 1](./src/images/ms01testscustom.png)

## **Milestone 02**

A segunda **Milestone** teve como objetivo a criação de testes baseados em propriedades. Estes testes baseiam-se na ideia de obter valores de input automáticos e aleatórios, usar esses valores no algoritmo e testar os resultados. Assim, seria possível testemunhar a capacidade do algoritmo de lidar com valores de input mais inesperados e menos controlados. 

Para tal, foi necessária a criação de geradores para as entidades do dominio e para os Opaque Types, de forma a que os testes fossem capazes de analisar várias combinações de inputs (a maior parte inputs válidos, mas em algumas situações, inputs propositadamente inválidos), ao longo de múltiplas repetições.

Relativamente às propriedades a serem estabelecidas, foram requisitadas duas propriedades pelo enunciado, em concreto:

- Uma enfermeira não pode ser utilizada no mesmo _shift_ com diferentes _roles_.
- O _shift schedule_ deve agendar todos os requistidos para cada dia.

Para além dessas, foi requesitado aos elementos do grupo a análise do dominio do problema e desenvolver, no minimo, mais 4 propriedades. As propriedades desenvolvidas foram as seguintes:

- A restrição de nº de _shifts_ máximos por dia deve ser respeitada.
- O _Shift Schedule_ gerado deve ter o numero correto de dias.
- _Roles_ desconhecidas deve ser lançar um erro de dominio.
- A restrição de nº de dias de descanso minimos, por semana, deve ser respeitada.
- A repetição do algoritmo deve resultar sempre no mesmo horário gerado.

### **Geradores**

Os geradores são mecanismo que permitem a criação de uma série de valores (quer sejam classes ou opaque types), seguindo regras básicas. Por exemplo, um gerador de identificadores String seguirá a regra de tamanho desses Ids, escolhendo um valor controlado de caracteres e criando uma String válida com esse comprimento.

Foram criados os seguintes geradores para os simple types criados na primeira **Milestone**:

```
object SimpleGenerator extends Properties("Simple"):

  def genStringId: Gen[StringId]=
    for{
      n <- Gen.chooseNum(2,10)
      cl <- Gen.listOfN(n, Gen.alphaChar)
      id <- StringId.from(cl.mkString).fold(_ => Gen.fail, id => Gen.const(id))
    }yield id

  def genRole: Gen[Role]=
    for{
      cl <- Gen.oneOf("A", "B", "C")
      role <- Role.from(cl).fold(_ => Gen.fail, role => Gen.const(role))
    }yield role

  def genBadRole: Gen[Role]=
    for{
      cl <- Gen.oneOf("D", "E", "F")
      role <- Role.from(cl).fold(_ => Gen.fail, role => Gen.const(role))
    }yield role

  def genDayNumber: Gen[DayNumber]=
    for{
      value <- Gen.chooseNum(1,7)
      dayNumber <- DayNumber.from(value).fold(_ => Gen.fail, number => Gen.const(number))
    }yield dayNumber

  def genNurseName: Gen[NurseName]=
    for{
      n <- Gen.chooseNum(2,10)
      cl <- Gen.listOfN(n, Gen.alphaChar)
      nurseName <- NurseName.from(cl.mkString).fold(_ => Gen.fail, name => Gen.const(name))
    }yield nurseName

  def genNurseSeniority: Gen[NurseSeniority]=
    for{
      value <- Gen.chooseNum(1,5)
      nurseSeniority <- NurseSeniority.from(value).fold(_ => Gen.fail, seniority => Gen.const(seniority))
    }yield nurseSeniority

  def genPreferenceValue: Gen[PreferenceValue]=
    for{
      value <- Gen.chooseNum(-2,2)
      preferenceValue <- PreferenceValue.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    }yield preferenceValue

  def genMaxShiftsPerDay: Gen[MaxShiftsPerDay]=
    for{
      value <- Gen.chooseNum(2,6)
      maxShiftsPerDay <- MaxShiftsPerDay.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    }yield maxShiftsPerDay

  def genMinRestDaysPerWeek: Gen[MinRestDaysPerWeek]=
    for{
      value <- Gen.chooseNum(3,7)
      minRestDaysPerWeek <- MinRestDaysPerWeek.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    }yield minRestDaysPerWeek
```

Estes geradores de simple types foram usados na criação de geradores para todas as diferentes entidades do dominio. De seguida, como exemplo disto, podemos visualizar os geradores criados relativamente a um **NurseRequirement**:

```
object NurseRequirementGenerator extends Properties("NurseRequirements"):
  def genNurseRequirement(ln: List[Nurse]) : Gen[NurseRequirement] =
    val roles = ln.flatMap(nurse => nurse.roles)
    for{
      role <- Gen.oneOf(roles)
      num = ln.filter(n => n.roles.exists(rol => rol.toString == role.toString))
      number <- Gen.chooseNum(1,num.size)
    } yield NurseRequirement(role, number)
  
  def genListOfNurseRequirements(ln: List[Nurse]) : Gen[List[NurseRequirement]] =
    for {
      n <- Gen.chooseNum(1,3)
      lp = (1 to n).map(i => genNurseRequirement(ln)).toList
      list <- Gen.sequence[List[NurseRequirement], NurseRequirement](lp)
    } yield verifyRequirements(list, ln)

  def genBadNurseRequirement : Gen[NurseRequirement] =
    for{
      role <- genBadRole
      number <- Gen.chooseNum(1,3)
    } yield NurseRequirement(role, number)

  def genBadListOfNurseRequirements : Gen[List[NurseRequirement]] =
    for {
      n <- Gen.chooseNum(1,3)
      lp = (1 to n).map(i => genBadNurseRequirement).toList
      list <- Gen.sequence[List[NurseRequirement], NurseRequirement](lp)
    } yield list

  def verifyRequirements(lst: List[NurseRequirement], ln: List[Nurse]) : List[NurseRequirement] =
    val lnn = ln.sortWith(_.name.toString < _.name.toString).toSet
    val newObject = (Set[NurseRequirement](), lnn)
    val res = lst.sortWith(_.role.toString < _.role.toString).foldLeft(newObject){
      (acc, req) =>  
        val act = acc._2.filter(n => n.roles.exists(role => role.toString == req.role.toString)).toList.sortWith(_.name.toString < _.name.toString)
        val act2 = acc._1.exists(r => r.role.toString == req.role.toString)
        if(act.size >= req.number && !act2)
          val a1 = acc._1 ++ Set(req) 
          val a2 = acc._2 -- act.take(req.number)  
          (a1,a2)
          
        else
          acc
    }
    
    res._1.toList
```

É de realçar que foram desenvolvidos geradores tanto para casos de sucesso, como o _genListOfNurseRequirements_ no excerto acima, que gera uma lista de requerimentos válidos, como para casos de insucesso, também no excerto acima, no gerador _genBadListOfNurseRequirements_, que cria uma lista de requerimentos que referem roles que nenhuma enfermeira possui (este gerador será usado, por exemplo, para testar a capacidade do algoritmo de detetar roles desconhecidos, de acordo com a quinta propriedade listada na secção anterior).


### **Propriedades**

Nesta secção serão apresentadas todas as propriedades desenvolvidas, de uma forma pormenorizada.

É importante referir que as propriedades utilizam os geradores desenvolvidos, de forma a trabalhar-se com informação de entrada aleatória e, dessa forma, garantir que o algoritmo funciona independentemente dos valores de entrada fornecidos.

Como apresentado previamente, desenvolveram-se as duas propriedades que foram requisitadas, e 5 propriedades adicionais. Relativamente às 5 propriedades que se desenvolveram para além das fornecidas, todas foram implementadas de forma a testar o output gerado pelo algoritmo, ou seja, definiu-se como prioridade criar-se propriedades para o resultado do algoritmo uma vez que não acrescentaria valor realizar-se propriedades para as entidades de entrada.

Antes de apresentarmos cada propriedade desenvolvida, é importante realçar que todas as propriedades utilizam o gerador **genScheduleInfo** que é responsável por utilizar os geradores de todas as entidades do dominio (quanto à informação de _input_) e criar multiplos _ScheduleInfo_. Dessa forma garante-se que todas as propriedades, ao utilizarem os _ScheduleInfo_ gerados, são testadas para informação de entrada aleatória.


```
  def genScheduleInfo : Gen[ScheduleInfo] =
    for {
      nurses <- genListOfNurses
      periods <- genListOfSchedulingPeriods(nurses)
      constraints <- genConstraint
      dayP <- genListOfDayPreferences
      periodP <- genListOfPeriodPreferences
    } yield ScheduleInfo(periods.toSet, nurses.toSet, constraints, dayP.toSet, periodP.toSet)
```


De seguida iremos apresentar cada uma, das 7 propriedades implementadas, individualmente.

#### **Uma enfermeira não pode ser utilizada no mesmo _shift_ com diferentes _roles_**

Esta propriedade verifica se uma enfermeira não foi colocada, pelo algoritmo, no mesmo _shift_ mas com roles diferentes.


```
  property("Nurse cannot be used in the same shift with different roles") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        r <- checkIfNurseIsNotRepeated(shiftSchedule)
      } yield r
    result.fold(_ => false, _ => true)
  )

  def checkIfNurseIsNotRepeated(shiftSchedule: ShiftSchedule) : Result[Boolean] =
    shiftSchedule.shiftDays.foldLeft[Result[Boolean]](Right(true))(
      (result1, days) =>
        days.shifts.foldLeft[Result[Boolean]](Right(true))(
          (result2, shifts) =>
            if (shifts.nurses.map(n => n.name).toSet.size != shifts.nurses.size) Left(DuplicateNurses) else result2
      )
    )
```


#### **O _shift schedule_ deve agendar todos os requerimentos para cada dia**

Esta propriedade verifica se o horário gerado respeita todos os requerimentos(_nurseRequirements_) para cada dia e para cada _shift_ que estava presente no _ScheduleInfo_.


```
  property("The shift schedule must schedule all the requirements for each day") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        r <- checkIfRequirementsAreMet(shiftSchedule, info)
      } yield r
    result.fold(_ => false, _ => true)
  )

  def checkIfRequirementsAreMet(shiftSchedule: ShiftSchedule, info: ScheduleInfo) : Result[Boolean] =
    shiftSchedule.shiftDays.foldLeft[Result[Boolean]](Right(true))(
      (result1, days) =>
        days.shifts.foldLeft[Result[Boolean]](Right(true))(
          (result2, shifts) =>
            val requirements = info.schedulingPeriods.filter(p => p.id.toString == shifts.id.toString).head.nurseRequirement
            requirements.foldLeft[Result[Boolean]](Right(true))(
              (result3, req) =>
                if(shifts.nurses.filter(n => n.role.toString == req.role.toString).size != req.number) Left(DuplicateNurses) else result2
            )
        )
    )
```


#### **A restrição de nº de _shifts_ máximos por dia deve ser respeitada**

Esta propriedade valida se não existe nenhuma enfermeira que tenha sido atribuida a um nº de _shifts_ superior ao nº máximo de _shifts_ por dia definido.


```
  property("Max shifts per day constraint must be respected") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        res <- checkIfMaxShiftPerDayConstraintIsRespected(shiftSchedule, info)
      } yield res
    result.fold(_ => false, _ => true)
  )

  def checkIfMaxShiftPerDayConstraintIsRespected(shiftSchedule: ShiftSchedule, info: ScheduleInfo) : Result[Boolean] =
    shiftSchedule.shiftDays.foldLeft[Result[Boolean]](Right(true)) {
      (res, day) =>
        val nursesSet = day.shifts.map(s => s.nurses).flatten
        val nursesList = day.shifts.toList.map(s => s.nurses.toList).flatten

        nursesSet.foldLeft[Result[Boolean]](Right(true)) {
          (res2, nurse) =>
            val count = nursesList.map(n => n.name.toString == nurse.name.toString).size
            if(MaxShiftsPerDay.compareMaxShiftsPerDay2(count, info.constraints.maxShiftsPerDay)) Left(InvalidMaxShiftsPerDay("Max Shifts per day constraint is not respected.")) else res2
        }
    }
```


#### **O _Shift Schedule_ gerado deve ter o numero correto de dias**

Esta propriedade verifica se o horário produzido possui todos os 7 dias da semana, como esperado.


```
  property("Shift Schedule has the right amount of days") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        res <- checkIfShiftScheduleHasRightAmountOfDays(shiftSchedule)
      } yield res
    result.fold(_ => false, _ => true)
  )

  def checkIfShiftScheduleHasRightAmountOfDays(shiftSchedule: ShiftSchedule) : Result[Boolean] =
    val distinctDaysIds = shiftSchedule.shiftDays.foldLeft[Set[String]](Set()){
      (acc, shiftDay) =>
        if(acc.contains(shiftDay.id.toString)) acc else acc ++ Set(shiftDay.id.toString)
    }
    if (distinctDaysIds.size != 7) Left(InvalidDaysQuantity) else Right(true)
```


#### **_Roles_ desconhecidas deve ser lançar um erro de dominio**

Esta propriedade valida se não existe nenhuma role para qual não exista uma nurse com essa mesma role.


```
  property("Unknown roles must always throw a domain error") = forAll(genFaultyScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- checkForUnknownRole(getShiftSchedule(info))
      } yield shiftSchedule
    result.fold(_ => false, _ => true)
  )

  def checkForUnknownRole(shiftSchedule: Result[ShiftSchedule]) : Result[Boolean] =
    shiftSchedule match
      case Left(value) => Right(true)
      case Right(value) => Left(UnknownRolesNotDetected)
```


#### **A restrição de nº de dias de descanso minimos, por semana, deve ser respeitada**

Esta propriedade valida se não existe nenhuma enfermeira que não possua um número de dias sem _shifts_ atribuidos maior ou igual ao nº minimo de dias de descanso definido.


```
  property("Min rest days per week must be respected") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        res <- checkIfMinRestDaysPerWeekConstraintIsRespected(shiftSchedule, info)
      } yield res
    result.fold(_ => false, _ => true)
  )

  def checkIfMinRestDaysPerWeekConstraintIsRespected(shiftSchedule: ShiftSchedule, info: ScheduleInfo) : Result[Boolean] =
    shiftSchedule.shiftDays.foldLeft[Result[Boolean]](Right(true)) {
      (res, day) =>
        val nursesSet = day.shifts.map(s => s.nurses).flatten
        nursesSet.foldLeft[Result[Boolean]](Right(true)) {
          (res2, nurse) =>
            val count = shiftSchedule.shiftDays.count(shiftDay => shiftDay.shifts.exists(shift => shift.nurses.exists(nur => nur.name == nurse.name)))
            if ((count + MinRestDaysPerWeek.toInt(info.constraints.minRestDaysPerWeek)) <= 7) res2 else Left(InvalidMinRestDays("InvalidMinRestDays"))
        }
    }
```


#### **A repetição do algoritmo deve resultar sempre no mesmo horário gerado**

Esta propriedade verifica se para os mesmos dados de entrada o horário gerado é sempre o mesmo ou seja, se existe Idempotência.


```
  property("Repetition of the algorithm must result in the same schedule") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule1 <- getShiftSchedule(info)
        shiftSchedule2 <- getShiftSchedule(info)
        res <- areSchedulesEquals(shiftSchedule1, shiftSchedule2)
      } yield res
    result.fold(_ => false, _ => true)
  )

  def areSchedulesEquals(shiftSchedule1: ShiftSchedule, shiftSchedule2: ShiftSchedule) : Result[Boolean] =
    val compare = shiftSchedule1 == shiftSchedule2
    compare match
      case true => Right(true)
      case false => Left(UnknownRolesNotDetected)
```

## **Milestone 03**

A terceira e última **Milestone** tem como objetivo incorporar as restrições quanto aos dias de descanso e as preferências das enfermeiras no algoritmo, produzindo um horário que maximiza as preferências das enfermeiras enquanto se respeita as restrições existentes.

Para esta **Milestone** deve-se considerar os seguintes requisitos:

- A senioridade de uma enfermeira influencia as preferências ( Preferência é o produto entre a senioridade e o valor definido como preferencia).
- O algoritmo deve produzir um horário de escalonamento que maximiza as preferências das enfermeiras.


### **Preferências das Enfermeiras**

Primeiramente, foi necessário desenvolver mecanismos que ordenassem uma lista de enfermeiras tendo em conta a ordem em que seriam consideradas para um requerimento. Esta ordem teria de prioritizar as preferências, de forma a que, para uma dada situação (para um certo dia e período), as enfermeiras que mais a preferissem estariam no topo da lista.

Para tal, foi criada uma função que, recebendo uma lista de enfermeiras, a identificação de um dia e de um período e as listas de preferências de dias e períodos, devolve-se uma versão da lista de enfermeiras, ordenadas pelas seguintes condições:

- Preferência do dia
- Preferência do período
- Número de papeis
- Ordem alfabética de nome

Essencialmente, as condições listadas acima, naquela ordem específica, ditam os critérios usados para ordenar, e desempatar, caso seja necessário, a prioridade de uma enfermeira para um certo período num certo dia. Por exemplo, se estiver a selecionar as enfermeiras para o período da manhã do dia 2, as enfermeiras com preferências (mais elevadas) para esse dia terão as posições superiores na lista ordenada. Se duas enfermeiras tiverem o mesmo valor de preferência de dia 2, serão usados os valores de preferência de período, ou seja a preferências das manhãs, para desempatar e escolher a superior. Caso possuam o mesmo valor de preferência de período, será escolhida a enfermeira com menos papeis, ou seja, a menos versátil. Caso tenham o mesmo número de papéis, será usada a ordem alfabética dos seus nomes como critério final.

Em termos de implementação destes conceitos, as funções geradas apresentam-se no seguinte excerto, sendo _getDayPreference_ e _getPeriodPreference_ responsáveis por localizar a preferência de dia e período de uma enfermeira, nas listas de preferências, e usadas por _generateNursesOrderedByFactor_ ao percorrer a lista de enfermeiras, determinando as suas preferências, multiplicando cada pela senioridade e colocando-as no set acumulado. Cada elemento deste set será um Tuple com 3 elementos: a enfermeira, o valor final de preferência de dia e o valor final de preferência de período. Esta lista de Tuples é consequentemente ordenada usando os quatro critérios mencionados anteriormente, e a lista de nurses segundo esta ordem é devolvida:

```
  def getDayPreference(dayPreferenceList: Set[DayPreference], nurse: Nurse, day: DayNumber): Int =
    val list = dayPreferenceList.filter(pref => DayNumber.toInt(pref.DayValue) == DayNumber.toInt(day) && pref.NurseId.toString() == nurse.name.toString())
    if(list.size > 0) 
      val pref = list.head 
      PreferenceValue.getValue(pref.Value)
    else 
      0

  def getPeriodPreference(periodPreferenceList: Set[PeriodPreference], nurse: Nurse, period: StringId): Int =
    val list = periodPreferenceList.filter(pref => pref.PeriodId == period && pref.NurseId.toString() == nurse.name.toString())
    if(list.size > 0) 
      val pref = list.head 
      PreferenceValue.getValue(pref.Value)
    else 
      0

  def generateNursesOrderedByFactor(nurseList: Set[Nurse], day: DayNumber, period: StringId, dayPreferenceList: Set[DayPreference], periodPreferenceList: Set[PeriodPreference]) : List[Nurse] =
    val tupleList = nurseList.foldLeft[Set[(Nurse, Int, Int)]](Set()) {
      (acc, nurse) =>
        val prefDay = getDayPreference(dayPreferenceList, nurse, day) * NurseSeniority.getValue(nurse.seniority)   
        val prefPeriod = getPeriodPreference(periodPreferenceList, nurse, period) * NurseSeniority.getValue(nurse.seniority)

        acc ++ Set((nurse, prefDay, prefPeriod))
    }
    tupleList.toList.sortBy(u => (-(u._2), -(u._3), u._1.roles.size, u._1.name.toString)).map(_._1).toList

```

Assim, _generateNursesOrderedByFactor_ será invocado sempre que o escalonamento de enfermeiras para um período, num certo dia, for executando, sendo a lista ordenada resultante usada para fazer as escolhas de associar a enfermeira ao requerimento ou não, garantindo que as enfermeiras com maior preferência para aquelas circunstãncias serão consideradas primeiro.


### **Constraint de Dias de Descanso**

Para conseguir adaptar esta consideração do algoritmo, primeiro foi necessário ajustar o mesmo para que cria-se os 7 dias consecutivamente, ao contrário do que estava a fazer, que era criar um dia único e repetir para todos os 7 dias.

Para tal, como pode ser visto no excerto em baixo, foi criada uma lista de dias, numerados de 1 a 7, e por cada dia, vai ser executada a criação de shifts que estava a ser feita nas milestones anteriores. O resultado deste ciclo vai ser verificado de forma a aferir que foram criados 7 dias com sucesso, caso contrário, é lançado erro.

```
        val dayNumbers = List(DayNumber.from2(1), DayNumber.from2(2), DayNumber.from2(3), DayNumber.from2(4), DayNumber.from2(5),DayNumber.from2(6), DayNumber.from2(7))

        val days = 
          dayNumbers.foldLeft[Set[ShiftDay]](Set()) {
            (acc, dayNumber) =>
              val s: Set[Shift] = si.schedulingPeriods.foldLeft[Set[Shift]](Set()){
                (acc2, sp) =>
                  val nursesOrderedByDay = generateNursesOrderedByFactor(si.nurses, dayNumber, sp.id, si.dayPreferences, si.periodPreferences)
                  
                  createShifts(acc2, si.constraints, sp.id, nursesOrderedByDay, sp.nurseRequirement, acc) match
                    case Left(value) => acc2
                    case Right(value) => acc2 ++ Set(value)
              }
              
              ShiftDay.from(dayNumber, s, si.schedulingPeriods.size) match
                case Left(value) => acc
                case Right(value) => acc ++ Set(value)
                
          }

        if(days.size<7 || days.filter(d => d.shifts.size == 0).size > 0)
          Left(DomainError.NotEnoughNurses)
        else
          ShiftSchedule.from(days)

```

Agora, como cada dia é escalonado consequentemente, temos de considerar que é necessário haver dias de descanso para as enfermeiras, sendo que sem essa verificação, podem haver resultados que incluem enfermeiras a trabalhar todos os dias da semana. Essa verificação é feita em _createShifts_, junto com as verificações definidas na Milestone 1 (as verificações da constraint de número máximo de shifts que a enfermeira exerce num dia; verificação que a enfermeira não se encontra já no shift com outro papel; verificação de que já não foi atingido o número necessário de enfermerias para aquele requerimento). 

Como pode ser visto no seguinte excerto, em _createShifts_, por cada requerimento, antes de adicionar uma enfermeira da lista de candidatas ao grupo de enfermeiras do shift, existem as referidas validações (sendo os resultados guardadaos em a1, a2, a3 e a4), incluindo agora a validação _minRestDaysPerWeekValidation_. 

```
def createShifts(shifts: Set[Shift], constraints: Constraint, id: StringId, nurses: List[Nurse], nr: Set[NurseRequirement], days: Set[ShiftDay]): Result[Shift] =

    val sn: Set[ShiftNurse] = nr.foldLeft[Set[ShiftNurse]](Set()){
      (acc, req) =>
        val filteredNurses = nurses.filter(n => n.roles.exists(role => role.toString == req.role.toString))//.toList.sortWith(_.name.toString < _.name.toString)
        val tmp = filteredNurses.foldLeft[Set[ShiftNurse]](Set()){
          (nurses, nur) =>
            val nameString = nur.name
            val a1 = maxShiftsPerDayValidation(constraints, shifts, nameString)
            val a2 = minRestDaysPerWeekValidation(constraints, days, shifts, nameString)
            val a3 = !nurseAlreadyInShiftValidation(acc, nameString)
            val a4 = !nurseRequirementMet(nurses, req.role, req.number)

            if(a1 && a2 && a3 && a4)
              nurses ++ Set(ShiftNurse(nameString, req.role))
            else
              nurses
        }
        acc ++ tmp
    }
    val sumNumber = nr.foldLeft[Int](0){(sum, rr) => sum + rr.number}
    Shift.from(id,sn, sumNumber)


```

No excerto seguinte, encontra-se a função _minRestDaysPerWeekValidation_ usada anteriormente. Nela, serão percorridos os dias anteriormente criados e incrementado um contador (o valor acumulado do foldLeft) cada vez que a enfermeira em questão é encontrada associada a um shift de um desses dias. Assim, saber-se-á o número de dias nos quais a enfermeira está a trabalhar, sendo esse valor posteriomente comparado ao ditado pela constraint MinRestDaysPerWeek e um valor boolean devolvido, indicando se esse valor já não é o limite, ou seja, pode ser excedido.

```

  def minRestDaysPerWeekValidation(constraint: Constraint, days: Set[ShiftDay], shifts: Set[Shift], nurseName: NurseName): Boolean =
    val count1 = 
      days.foldLeft[Int](0) {
        (acc, day) =>
          val nursesList = day.shifts.count(shift => shift.nurses.exists(nurse => nurse.name == nurseName))
          if(nursesList>0) acc+1 else acc
      }

    MinRestDaysPerWeek.compareMinRestDaysPerWeek(7-count1, constraint.minRestDaysPerWeek)

```


Tendo isto, estará a ser feito o escalonamento individual de cada dia, cumprindo-se agora a constraint de dias de descanso e tendo em consideração as preferências, respeitando tudo o que era imposto por esta Milestone.

### **Heurísticas**

Embora os objetivos da Milestone 3 tenham já sido atingidos, com os testes XML que foram eventualmente fornecidos, surgiu um novo problema: a necessidade de heurísticas para posicionar as enfermeiras. Isto pois, até agora, o posicionamento de enfermeiras têm seguido uma estruturação _First Fit_, ou seja, a primeira enfermeira que passasse a todas as constraints e todas as outras validações, seria escolhida para responder a um certo requerimento. Contudo, segundo os resultados esperados por estes testes XML, o algoritmo teria agora de estar pronto para fazer uma escolha "inteligente", de forma a que, possivelmente, as preferências sejam desrespeitadas, se for necessário colocar uma enfermeira específica num papel, com a enfermeira que seria escolhida normalmente sendo guardada para um dia posterior, de forma a criar um schedule estável e que não sofresse, por exemplo, o erro de _NotEnoughNurses_.

Para tal, é necessário uma heurística capaz de percorrer várias combinações e proporcionar este tipo de escolhas, tudo enquanto respeita as preferências, dentro do possível.

Foram consideradas duas possíveis heurísticas, tendo sidas ambas implementadas:

- **Revisão e Substituição**: Esta metodologia segue o princípio de que o algoritmo primeiro irá decorrer de forma normal, como foi pensado até agora, obtendo um resultado que corresponde ou a um schedule estável/completo (sendo nesse caso esse schedule devolvido) ou um schedule incompleto e com falta de enfermeiras. Caso esta segunda situação se verifique, o algoritmo, antes de desistir por completo e devolver erro, vai tentar rever este resultado errado e vai percorré-lo dia a dia, começando com o último que foi gerado, tentando restruturar esse mesmo dia e prosseguir com o escalonamento dos dias que o seguem, tentando completar os 7. Caso as várias combinações desse dia não tenham impacto positivo nos restantes dias, ele vai tentar repetir o processo no dia anterior, até esgotar dias para rever e desistir por completo de tentar arranjar este resultado. Por exemplo, caso o escalonamento tenha apenas conseguido criar os dias 1 ao 5, ele vai analisar o dia 5, tenta outra possível combinação válida de enfermeiras para esse dia e volta a tentar fazer o dia 6. Caso não funcione, tentará outras combinações do dia 5. Se nenhuma combinação do dia 5 funcionar, ele vai dar um passo atrás e irá tentar restruturar o dia 4 e consequentemente refazer o dia 5, 6 e 7, segundi esta lógica até ao dia 1, no pior dos casos.

- **Look Ahead**: Esta heurística consiste em olhar para as possíveis consequências de associar uma enfermeira a um requerimento, olhando para a frente, vendo se será possível gerar o schedule por completo se ela for escolhida, fazendo assim uma escolha informada sobre essa enfermeira. Para tal, o método irá assumir que a enfermeira é escolhida e envia toda a informação que acumulou sobre o schedule até o momento, mais a informação desta escolha, a um outro método que irá tentar recursivamente criar o restante do schedule com estas informações. Caso esta recursividade obtenha um resultado positivo, o método original saberá que inserindo esta enfermeira nesta posição perimitirá que o schedule seja gerado, sendo então a enfermeira escolhida. Se a recursividade falhar, o método original reagirá descartando a enfermeira e tentando a candidata seguinte. Desta forma, sempre que ele considera inserir uma enfermeira no schedule, o algoritmo olhará para o futuro possível de acordo com esta escolha, sendo uma decisão feita de acordo com essa informação.

Nas seguintes secções serão analisadas as escolhas de desenho e implementação destas heurísticas:

#### **Heurística de Revisão e Substituição**

A implementação desta heurística resultou num algoritmo muito mais complexo e longo. Tendo isto em conta, para efeitos de relatório e explicação da lógica aplicada, foi elaborado o diagrama em baixo, representando a série de eventos que decorrem para efetuar os príncipios defendidos por esta heurística:


![Flowchart da Heurística](./src/diagrams/backtracking.drawio.png)

O início do diagram representa a lógica básica das funcionalidades de escalonamento que antecedem a heurística:

- Há uma verificação de Unknown Roles na informação de input e lançado erro caso se verifique;
- O escalonamento principal, no qual por cada dia da semana, e consequentemente por cada período vindo de input, vai ser obtida a lista de enfermeiras ordenadas (como foi explicado numa secção anterior) e essa lista é usada para criar um shift para o período a ser analisado. Nessa criação de shift, cada requerimento do período correspondente é visto, sendo a lista de enfermerias filtrada de forma a conter apenas as enfermeiras com o papel pedido pelo requerimento. A lista filtrada é percorrida, com cada enfermeira sendo verificada relativamente às constraints e outras validações, sendo adicionada ao shift caso passe. Cada Shift criado é posteriormente usado para gerar cada dia do Schedule final.
- O resultado do escalonamento é verificado, de forma a confirmar que foram criados 7 dias e que não houve um dia a falhar, dado falta de nurses.

Aqui entra então a questão desta heurística:

- Se o resultado do escalonamento inicial for válido, o schedule é criado e devolvido. Caso contrário, vai ser invocado a função de retry, sendo o resultado do escalonamento inicial enviado para esta.
- O retry vai primeiro determinar qual foi o dia problemático, ou seja o primeiro dia que falhou escalonamento, e os dias estáveis, ou seja, todos os anteriores ao dia problemático. Vai também determinar o período e o requerimento que causaram a falha do dia problemático e a lista de enfermeiras que o algoritmo tentou usar para escalonar esse requerimento, sem sucesso.
- De seguida, o retry vai iniciar com uma tentativa de refazer o dia que falhou de forma diferente: fazendo o seu escalonamento por ordem inversa de períodos. Quer isto dizer que, se existirem três períodos - night, morning e afternoon - e se o dia 6 foi o primeiro dia a falhar no resultado, ele irá retentar o escalonamento do dia 6, mantendo todas as informações do dia 1 a 5 do resultado, mas desta vez seguindo a ordem inversa dos períodos - afternoon, morning, night - para o tratamento do dia 6.
- Caso esta primeira tentativa de arranjo funcionar, os dias que o seguem serão escalonados de forma normal (ordem natural dos períodos). Se isto funcionar, será devolvido o schedule resultante, evitando a necessidade de rever dias já escalonados com sucesso. 
- Caso falhe, o retry vai prosseguir com outra estratégia. Por cada dia anterior ao dia problemático, será percorrida a lista de enfermeiras que o dia problemático tentou usar para responder ao requerimento que falhou, e que se encontravam indisponíveis devido à constraint de MinRestDaysPerWeek. Será feita uma revisão desse dia anterior ao problemático, de forma a recriar os seus shifts sem a nurse em questão. Se se conseguir recriar o dia desta forma, procede-se com o escalonamento normal dos dias que o seguem (incluindo o dia problemático) tentando cumprir os 7 dias. Se tiver sucesso, é devolvido este novo set de dias. Se falhar, vai tentar este processo outra vez, com a próxima enfermeira da lista. Se a oclusão das enfermeiras dos shifts desse dia não levar a um set de  7 dias válidos, ele repete o processo com o dia anterior ao dia anterior ao dia problemático. Por exemplo, se o dia 6 for o dia problemático e a lista de nurses que tentou usar sem sucesso foi N1, N2 e N3, ele vai tentar primeiro fazer o dia 5 sem usar N1 e vê se consegue criar o dia 6; tentar fazer o dia 5 sem usar N2 e vê se consegue criar o dia 6; tentar fazer o dia 5 sem usar N3 e vê se consegue criar o dia 6; tendo esgotado as possíveis revisões de 5, vai repetir o processo com o dia 4, ocultando as mesmas enfermeiras e tentando refazer o dia 5 e depois o dia 6. É importante notar que, se o algoritmo conseguir fazer 6 mas não conseguir fazer 7, tendo em conta este mesmo exemplo, ele vai continuar as tentativas a partir deste resultado visto que apresenta ser uma melhoria relativamente ao original (possui 6 dias estáveis em vez de 5). 
- Se ele tiver revisto todos os dias anteriores ao dia problemático sem sucesso, ou seja, se ele não conseguir sequer rever o dia 1, admite que é impossível solucionar este escalonamento.


#### **Heurística de Look Ahead**

A Heurística de Look Ahead apresenta maior complexidade de processamento, mas é mais simples de preparar, explicar e apresentar.

A sua lógica reside, como já foi mencionado, no algoritmo, durante o escalonamento principal, prever as consequências da escolha de uma enfermeira para um requerimento.
Com isto, o método descrito na Milestone 1 que criava os dias do Schedule, _getShiftSchedule_, e o método que criava Shifts a partir da lista de requerimentos de um período, _createShifts_, vão ter de sofrer alterações, não só para permitirem recursividade, mas para poderem executar o escalonamento a partir de qualquer dia, período e requerimento, e não apenas do início exato da semana, como o algoritmo original:

```
  def getShiftScheduleV2(si: ScheduleInfo, sDays: Set[ShiftDay], sShifts: Set[Shift], sNurses: Set[ShiftNurse], reqNurses: Set[ShiftNurse]): Result[ShiftSchedule] =
          
    val difference = checkForUnknownRoles(si)

    difference.size!= 0 match
      case true => Left(DomainError.UnknownRequirementRole(difference.head))

      case false =>   
        val dayToStart = sDays.size + 1;
        val periodToStartNumber = sShifts.size;
        val periodToStart = si.schedulingPeriods.toList(periodToStartNumber);
        val reqToStart = findRequirement(periodToStart.nurseRequirement, reqNurses.size);
            
        val dayNumbers = (dayToStart to 7).foldLeft[List[DayNumber]](List[DayNumber]()){
          (acc, n) => 
            val nl: List[DayNumber] = acc :+ DayNumber.from2(n)
            nl
        }


        val daysP = 
          dayNumbers.foldLeft[(Boolean, Set[ShiftDay])](true, sDays) {
            (acc, dayNumber) =>
              if(acc._1)
                val s = si.schedulingPeriods.foldLeft[(Boolean, Int, Set[Shift])]((true, 0, sShifts)){
                  (acc2, sp) =>         
                    if(acc2._1)         
                      if((dayNumber same dayToStart) && acc2._2<periodToStartNumber)
                        (acc2._1, acc2._2 + 1, acc2._3)
                      else                    
                        val nursesOrderedByDay = generateNursesOrderedByFactor(si.nurses, dayNumber, sp.id, si.dayPreferences, si.periodPreferences)
                        
                        createShiftsV2(si, reqToStart, sNurses, reqNurses, acc2._3, si.constraints, sp.id, nursesOrderedByDay, sp.nurseRequirement, acc._2) match
                          case Left(value) => 
                            (false, acc2._2 + 1, acc2._3)
                          case Right(value) =>
                            (acc2._1, acc2._2 + 1, (acc2._3 ++ Set(value)))
                    else
                      acc2
                }
                
                ShiftDay.from(dayNumber, s._3, si.schedulingPeriods.size) match
                  case Left(value) => 
                    (false, acc._2)
                  case Right(value) => (acc._1, acc._2 ++ Set(value))
              
              else
                acc
                
          }

        val days = daysP._2

        if(days.size<7 || days.filter(d => d.shifts.size == 0).size > 0)
          // println(days)
          Left(DomainError.NotEnoughNurses)
        else
          // println(days)
          ShiftSchedule.from(days)



```

O método _getShiftScheduleV2_ receberá não só a informação de input (_ScheduleInfo_), mas também um set de dias, um set de Shifts e dois sets de enfermeiras, todos já estabelecidos com o intuito de constituir um ShiftSchedule. A lógica será ter o método a usar estes sets para localizar a partir de que ponto deve retomar o escalonamento.

Para tal, após a verificação de Unknown Roles na informação de input, algo que já foi explicado numa secção anterior, o método vai usar o set de dias para saber quantos dias já estão escalonados, e, consequentemente, que dia vai retomar o escalonamento. A mesma lógica aplica-se ao set de shifts, sabendo assim quantos shifts (ou seja quantos períodos) é que já foram abordados por completo no dia que se encontra em tratamento. Os dois sets de enfermeiras, sendo o primeiro dedicado às enfermeiras associadas ao shift atualmente a ser definido, mas de requerimentos anteriores, e o segundo set a enfermeiras desse mesmo shift, mas do requerimento atualmente a ser tratado, vão ser usadas para identificar esse requerimento, através das suas contagens e da lista de requerimento do período a ser analisado de momento.

Tendo-se localizado, o método irá criar uma lista de dias desde o dia atualmente a ser escalonado até sete e percorrerá essa lista, de forma a escalonar o dia atual e todos os seguintes. No valor inicial do acumulado do foldLeft exercido na lista de dias é inserido o set de dias recebido por parâmetro, de forma a incluir nele os dias que serão gerados.

Por cada dia, ele percorre a lista de periodos proveniente de input, sendo que para o dia que está a ser retomado, ele vai ingorar todos os períodos que já foram tratados. Para cada período, vai ser obtida a lista ordenada de enfermeiras e o método _createShiftsV2_ invocado.

```
def createShiftsV2(si: ScheduleInfo, reqNumber: Int, sNurses: Set[ShiftNurse], reqNurses: Set[ShiftNurse], shifts: Set[Shift], constraints: Constraint, id: StringId, nurses: List[Nurse], nr: Set[NurseRequirement], days: Set[ShiftDay]): Result[Shift] =

    val sn = nr.foldLeft[(Int, Set[ShiftNurse])]((0, sNurses)){
      (acc, req) =>
        if(acc._1 < reqNumber)
          (acc._1 + 1, acc._2)
        else

          val filteredNurses = nurses.filter(n => n.roles.exists(role => role.toString == req.role.toString))
          
          val nnn = getNurses(acc._1, reqNumber, reqNurses)
          val tmp = filteredNurses.foldLeft[Set[ShiftNurse]](nnn){
            (nnurses, nur) =>
              val nameString = nur.name
              val a1 = maxShiftsPerDayValidation(constraints, shifts, nameString)
              val a2 = minRestDaysPerWeekValidation(constraints, days, shifts, nameString)
              val a3 = !nurseAlreadyInShiftValidation(acc._2++nnurses, nameString)
              val a4 = !nurseRequirementMet(nnurses, req.role, req.number)        

              if(a1 && a2 && a3 && a4)

                lookAhead(si, days, shifts, acc._2, nnurses ++ Set(ShiftNurse(nameString, req.role)), nr, id) match
                  case Left(value) => 
                    nnurses
                  case Right(value) => 
                    nnurses ++ Set(ShiftNurse(nameString, req.role))

              else
                nnurses
            }
          (acc._1 + 1, acc._2 ++ tmp)
      }
      val sumNumber = nr.foldLeft[Int](0){(sum, rr) => sum + rr.number}
      Shift.from(id,sn._2, sumNumber)


```

Este método vai receber do _getShiftScheduleV2_ a informação do período a escalonar, incluindo o seu ID e os seus requerimentos, mas também recebe o número do requerimento que deve retomar (caso haja algum para retomar) e as listas de enfermeiras já anteriormente descritas (que se associam ao período em questão e ao requerimento a ser retomado, respetivamente). Recebe ainda a informação acumulada até agora sobre o Schedule (dias e shifts já estabelecidos).

Com esta informação, os requerimentos vão ser percorridos, sendo qualquer requerimento já escalonado ignorado, e por cada um filtra-se as enfermeiras que o podem responder e cada uma destas enfermeiras vai sofrer a validação das constraints e outras verificações relevantes. Se a enfermeira for válida de ser associada a este requerimento, segundo estas validações, ela vai sofrer uma última confirmação através do método _lookAhead_. Este será contextualizado de seguida, mas essencialmente, vai usar tudo o que se sabe que é válido sobre o Schedule, e esta possível nova enfermeira, e prepará essa informação, obtendo um Schedule válido caso essa enfermeira fosse o último elemento para o completar, ou enviando esses dados para _getShiftScheduleV2_, de modo a verificar se é possível gerar um Schedule escolhendo esta enfermeira.

Se o resultado do _lookAhead_ for Right, significa que é possível gerar um Schedule válido com esta enfermeira neste papel, naquel shift, naquele dia, sendo a enfermeira consequentemente adicionada ao set de enfermeiras e o escalonamento continuando, de forma a completar todos os requerimentos, completar todos os shifts, completar todos os dias e obtendo o Schedule, usando sempre o lookAhead para cada potencial enfermeira. Se der Left, significa que, embora a enfermeira seja válida para esta posição, ficará ocupada e não poderá ocupar outra posição futura que possibilitará o Schedule válido, sendo assim ignorada e a próxima enfermeira considerada.

O método _lookAhead_ referido é o seguinte:

```
def lookAhead(si: ScheduleInfo, sDays: Set[ShiftDay], sShifts: Set[Shift], sNurses: Set[ShiftNurse], reqNurses: Set[ShiftNurse], nr: Set[NurseRequirement], id: StringId): Result[ShiftSchedule] =
    val nn = nr.foldLeft[Int](0){(sum, rr) => sum + rr.number}

    if(sDays.size == 7)
      ShiftSchedule.from(sDays)

    val res = if(sNurses.size + reqNurses.size == nn)
      Shift.from(id,sNurses++reqNurses,nn) match
        case Left(value) =>           
          Left(DomainError.NotEnoughNurses)
        case Right(value) =>
          if((sShifts.size + 1) == si.schedulingPeriods.size)
            ShiftDay.from(DayNumber.from2(sDays.size+1), sShifts ++ Set(value), si.schedulingPeriods.size) match
              case Left(value2) =>                
                Left(DomainError.NotEnoughNurses)
              case Right(value2) =>                  
                val nDays = sDays ++ Set(value2)
                if(nDays.size == 7)
                  ShiftSchedule.from(nDays)
                else
                  getShiftScheduleV2(si, nDays, Set(), Set(), Set())
          else
            getShiftScheduleV2(si, sDays, sShifts ++ Set(value), Set(), Set()) 
    else
      getShiftScheduleV2(si, sDays, sShifts, sNurses, reqNurses)

    res
```

Neste método, a informação acumulada sobre o Schedule é analisada, mais concretamente, o impacto da adição da enfermeira que levou à chamada do método. Verifica-se se com esta enfermeira:

- O Shift fica completo. Se esse não for o caso, a informação é simplesmente passada a _getShiftScheduleV2_ tal como foi recebida. 
- Se o Shift ficar completo, é adicionado ao set de Shifts e verifica-se se, com este novo Shift, o dia fica completo. 
- Se esse não for o caso, a informação é passada a _getShiftScheduleV2_, com este expandido set de Shifts e os sets de enfermeiras vazios. 
- Se o dia for completo com este novo Shift, o ShiftDay é criado e verifica se é o 7º dia. 
- Se não for, é passada a informação a _getShiftScheduleV2_ com a nova versão do set de dias e com os restantes sets vazio. 
- Se sim, será devolvido o Schedule criado com estes sete dias, ou seja, a operação de _lookAhead_ chega ao final.
- Estão também preparadas verificações da correta criação dos Shifts, ShiftsDays e ShiftSchedules, sendo que se ocorrer erro, é dado Left, deixando _createShiftsV2_ saber que esta enfermeira não é viável.

Estas operações garantem o progresso da informação que está a ser "prevista" pelo escalonamento principal e também garante as operações de paragem quando a enfermeira apresenta ser válida e um Schedule é eventualmente criado, ou inválida e a sua inclusão vai levar a um eventual erro.



### **Testes**

Para testar as adições ao algoritmo feitas nesta Milestone, foram desenvolvidos testes únitários para as novas funções, seguindo as mesmas estruturas que os testes da Milestone 1, e foram usados os ficheiros XML proporcionados.

Foram ainda desenvolvidos mais testes de integração usando novos ficheiros XML desenvolvidos pelo grupo para esse fim, sendo o algoritmo capaz de passar a todos:

![Testes Milestone 3](./src/images/ms03tests.png)


## **Conclusões**

Em suma, o algoritmo foi desenvolvido sendo capaz de escalonar os horários requisitados e ter em consideração não só os períodos e os seus requerimentos, mas também as constraints, validações e preferências necessárias para obter horários ideais. Desta forma, foram desenvolvidos e testados os objetivos listados nas três Milestones propostas. 

Possíveis melhorias incluem a limpeza de código, remoção de algumas redundâncias e simplificação de alguns processos. 