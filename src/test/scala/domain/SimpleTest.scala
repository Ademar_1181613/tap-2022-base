package domain

import domain.DomainError
import org.scalatest.funsuite.AnyFunSuite
import domain.Simple.*

class  SimpleTest extends AnyFunSuite:

  test("String id is empty"){
    val result = StringId.from("")
    assert(result.fold(error => error == DomainError.EmptyIdError("Invalid ID (empty)"), _ => false))
  }

  test("String id is blank"){
    val result = StringId.from("        ")
    assert(result.fold(error => error == DomainError.EmptyIdError("Invalid ID (empty)"), _ => false))
  }

  test("String id is created"){
    val result = StringId.from("identification")
    assert(result.fold(_ => false, _ => true))
  }

  test("Role is empty"){
    val result = Role.from("")
    assert(result.fold(error => error == DomainError.InvalidRole("Invalid role (empty) for Nurse"), _ => false))
  }

  test("Role is blank"){
    val result = Role.from("        ")
    assert(result.fold(error => error == DomainError.InvalidRole("Invalid role (empty) for Nurse"), _ => false))
  }

  test("Role is created"){
    val result = Role.from("role")
    assert(result.fold(_ => false, _ => true))
  }

  test("Day number is below 1"){
    val result = DayNumber.from(0)
    assert(result.fold(error => error == DomainError.InvalidDayValue("Day Number is not valid (not in the 1-7 interval)"), _ => false))
  }

  test("Day number is above 7"){
    val result = DayNumber.from(8)
    assert(result.fold(error => error == DomainError.InvalidDayValue("Day Number is not valid (not in the 1-7 interval)"), _ => false))
  }

  test("Day number is created"){
    val result = DayNumber.from(4)
    assert(result.fold(_ => false, _ => true))
  }

  test("NurseName is empty"){
    val result = NurseName.from("")
    assert(result.fold(error => error == DomainError.EmptyName, _ => false))
  }

  test("NurseName is blank"){
    val result = NurseName.from("        ")
    assert(result.fold(error => error == DomainError.EmptyName, _ => false))
  }

  test("NurseName is created"){
    val result = NurseName.from("Ana")
    assert(result.fold(_ => false, _ => true))
  }

  test("NurseSeniority is below 1"){
    val result = NurseSeniority.from(0)
    assert(result.fold(error => error == DomainError.SeniorityValueOutOfRange(0), _ => false))
  }

  test("NurseSeniority is above 5"){
    val result = NurseSeniority.from(6)
    assert(result.fold(error => error == DomainError.SeniorityValueOutOfRange(6), _ => false))
  }

  test("NurseSeniority is created"){
    val result = NurseSeniority.from(3)
    assert(result.fold(_ => false, _ => true))
  }

  test("PreferenceValue is created"){
    val result = PreferenceValue.from(2)
    assert(result.fold(_ => false, _ => true))
  }

  test("PreferenceValue is above 2"){
    val result = PreferenceValue.from(3)
    assert(result.fold(error => error == DomainError.PreferenceValueOutOfRange(3), _ => false))
  }

  test("PreferenceValue is below -2"){
    val result = PreferenceValue.from(-3)
    assert(result.fold(error => error == DomainError.PreferenceValueOutOfRange(-3), _ => false))
  }

  test("MaxShiftsPerDay is created"){
    val result = MaxShiftsPerDay.from(1)
    assert(result.fold(_ => false, _ => true))
  }

  test("MaxShiftsPerDay is below 0"){
    val result = MaxShiftsPerDay.from(-1)
    assert(result.fold(error => error == DomainError.InvalidMaxShiftsPerDay("Max shifts per day should be a positive number!"), _ => false))
  }

  test("MinRestDaysPerWeek is below 0"){
    val result = MinRestDaysPerWeek.from(-1)
    assert(result.fold(error => error == DomainError.InvalidMinRestDays("Min Rest Days Per Week should be between 0 and 7"), _ => false))
  }

  test("MinRestDaysPerWeek is above 7"){
    val result = MinRestDaysPerWeek.from(8)
    assert(result.fold(error => error == DomainError.InvalidMinRestDays("Min Rest Days Per Week should be between 0 and 7"), _ => false))
  }

  test("MinRestDaysPerWeek is created"){
    val result = MinRestDaysPerWeek.from(1)
    assert(result.fold(_ => false, _ => true))
  }




