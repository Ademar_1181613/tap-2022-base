package domain.generator

import scala.language.adhocExtensions
import org.scalacheck.*
import SimpleGenerator.*
import domain.nurse.Nurse
import domain.Simple.*

object NurseGenerator extends Properties("Nurses"):
  def genNurse : Gen[Nurse] =
    for {
      name <- genNurseName
      sen <- genNurseSeniority
      roles <- genRolesForNurse
    } yield Nurse(name, sen, roles.toSet)
  
  def genRolesForNurse : Gen[List[Role]] =
    for {
      n <- Gen.chooseNum(1, 3)
      list <- Gen.listOfN(n, genRole)
    } yield list.toList
  
  def genListOfNurses : Gen[List[Nurse]] =
    for {
      n <- Gen.chooseNum(1,10)
      lp = (1 to n).map(i => genNurse).toList
      list <- Gen.sequence[List[Nurse], Nurse](lp)
    } yield list


