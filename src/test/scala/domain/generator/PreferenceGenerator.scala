package domain.generator

import scala.language.adhocExtensions
import org.scalacheck.*
import SimpleGenerator.*
import domain.preference.DayPreference
import domain.preference.PeriodPreference

object PreferenceGenerator extends Properties("Preferences"):
  def genDayPreference : Gen[DayPreference] =
    for {
      pv <- genPreferenceValue
      si <- genStringId
      dn <- genDayNumber 
    } yield DayPreference(pv, si, dn)

  def genListOfDayPreferences : Gen[List[DayPreference]] =
    for {
      n <- Gen.chooseNum(1,6)
      lp = (1 to n).map(i => genDayPreference).toList
      list <- Gen.sequence[List[DayPreference], DayPreference](lp)
    } yield list


  def genPeriodPreference : Gen[PeriodPreference] =
    for {
      pv <- genPreferenceValue
      si <- genStringId
      ni <- genStringId 
    } yield PeriodPreference(pv, si, ni)

  def genListOfPeriodPreferences : Gen[List[PeriodPreference]] =
    for {
      n <- Gen.chooseNum(1,6)
      lp = (1 to n).map(i => genPeriodPreference).toList
      list <- Gen.sequence[List[PeriodPreference], PeriodPreference](lp)
    } yield list

 
    