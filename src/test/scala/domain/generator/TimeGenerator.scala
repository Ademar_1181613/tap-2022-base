package domain.generator

import scala.language.adhocExtensions
import org.scalacheck.*
import SimpleGenerator.*
import domain.schedulingperiod.Time

object TimeGenerator extends Properties("Times"):
  def genTime : Gen[Time] =
    for {
      hour <- Gen.chooseNum(0,23)
      min <- Gen.chooseNum(0,59)
      sec <- Gen.chooseNum(0,59)
    } yield Time(hour, min, sec)
  

