package domain.generator

import scala.language.adhocExtensions
import org.scalacheck.*
import SimpleGenerator.*
import domain.schedulingperiod.NurseRequirement
import domain.nurse.Nurse
import domain.Simple.*

object NurseRequirementGenerator extends Properties("NurseRequirements"):
  def genNurseRequirement(ln: List[Nurse]) : Gen[NurseRequirement] =
    val roles = ln.flatMap(nurse => nurse.roles)
    for{
      role <- Gen.oneOf(roles)
      num = ln.filter(n => n.roles.exists(rol => rol.toString == role.toString))
      number <- Gen.chooseNum(1,num.size)
    } yield NurseRequirement(role, number)
  
  def genListOfNurseRequirements(ln: List[Nurse]) : Gen[List[NurseRequirement]] =
    for {
      n <- Gen.chooseNum(1,3)
      lp = (1 to n).map(i => genNurseRequirement(ln)).toList
      list <- Gen.sequence[List[NurseRequirement], NurseRequirement](lp)
    } yield verifyRequirements(list, ln)

  def genBadNurseRequirement : Gen[NurseRequirement] =
    for{
      role <- genBadRole
      number <- Gen.chooseNum(1,3)
    } yield NurseRequirement(role, number)

  def genBadListOfNurseRequirements : Gen[List[NurseRequirement]] =
    for {
      n <- Gen.chooseNum(1,3)
      lp = (1 to n).map(i => genBadNurseRequirement).toList
      list <- Gen.sequence[List[NurseRequirement], NurseRequirement](lp)
    } yield list

  def verifyRequirements(lst: List[NurseRequirement], ln: List[Nurse]) : List[NurseRequirement] =
    val lnn = ln.sortWith(_.name.toString < _.name.toString).toSet
    val newObject = (Set[NurseRequirement](), lnn)
    val res = lst.sortWith(_.role.toString < _.role.toString).foldLeft(newObject){
      (acc, req) =>  
        val act = acc._2.filter(n => n.roles.exists(role => role.toString == req.role.toString)).toList.sortWith(_.name.toString < _.name.toString)
        val act2 = acc._1.exists(r => r.role.toString == req.role.toString)
        if(act.size >= req.number && !act2)
          val a1 = acc._1 ++ Set(req) 
          val a2 = acc._2 -- act.take(req.number)  
          (a1,a2)
          
        else
          acc
    }
    
    res._1.toList



    

