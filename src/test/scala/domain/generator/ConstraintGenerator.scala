package domain.generator

import scala.language.adhocExtensions
import org.scalacheck.*
import SimpleGenerator.*
import domain.constraint.Constraint

object ConstraintGenerator extends Properties("Constraints"):
  def genConstraint : Gen[Constraint] =
    for {
      msd <- genMaxShiftsPerDay
      mrw <- genMinRestDaysPerWeek 
    } yield Constraint(mrw, msd)
  
  def genListOfConstraints : Gen[List[Constraint]] =
    for {
      n <- Gen.chooseNum(1,6)
      lp = (1 to n).map(i => genConstraint).toList
      list <- Gen.sequence[List[Constraint], Constraint](lp)
    } yield list

    