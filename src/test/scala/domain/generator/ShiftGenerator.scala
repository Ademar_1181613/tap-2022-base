package domain.generator

import scala.language.adhocExtensions
import org.scalacheck.*
import org.scalacheck.Prop.forAll
import SimpleGenerator.*
import domain.shift.Shift
import domain.shift.ShiftNurse
import ShiftNurseGenerator.*

object ShiftGenerator extends Properties("Shifts"):

  def genShift(lsn: List[ShiftNurse]) : Gen[Shift] =
    for {
      id <- genStringId
      list <- genNursesForShift(lsn)
    } yield Shift(id, list.toSet)

  def genNursesForShift(lsn: List[ShiftNurse]) : Gen[List[ShiftNurse]] =
    for {
      n <- Gen.chooseNum(1, 10)
      list <- Gen.listOfN(n, Gen.oneOf(lsn))
    } yield list.toList

  def genListOfShifts(lsn: List[ShiftNurse]) : Gen[List[Shift]] =
    for {
      n <- Gen.chooseNum(1,6)
      lt = (1 to n).map(i => genShift(lsn)).toList
      list <- Gen.sequence[List[Shift], Shift](lt)
    } yield list
  
  property("Validate generator genShift") = forAll(genListOfShiftNurses)( lsn =>
    forAll(genShift(lsn))( shift =>
      shift.nurses.forall(n => lsn.contains(n))
    )
  )
  

  