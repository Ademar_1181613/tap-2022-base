package domain.generator

import scala.language.{adhocExtensions, postfixOps}
import org.scalacheck.*
import org.scalacheck.Prop.forAll
import SimpleGenerator.*
import domain.{Result, ScheduleInfo}
import domain.DomainError.*
import domain.Simple.*
import domain.shift.{Shift, ShiftNurse, ShiftSchedule}
import NurseRequirementGenerator.*
import SchedulingPeriodGenerator.*
import NurseGenerator.*
import ConstraintGenerator.*
import PreferenceGenerator.*
import algorithm.SchedulingAlgorithmUtils01.*

object ScheduleInfoGenerator extends Properties("ScheduleInfos"):

  def genScheduleInfo : Gen[ScheduleInfo] =
    for {
      nurses <- genListOfNurses
      periods <- genListOfSchedulingPeriods(nurses)
      constraints <- genConstraint
      dayP <- genListOfDayPreferences
      periodP <- genListOfPeriodPreferences
    } yield ScheduleInfo(periods.toSet, nurses.toSet, constraints, dayP.toSet, periodP.toSet)

  def genFaultyScheduleInfo : Gen[ScheduleInfo] =
    for {
      nurses <- genListOfNurses
      periods <- genBadListOfSchedulingPeriods
      constraints <- genConstraint
      dayP <- genListOfDayPreferences
      periodP <- genListOfPeriodPreferences
    } yield ScheduleInfo(periods.toSet, nurses.toSet, constraints, dayP.toSet, periodP.toSet)

  property("Nurse cannot be used in the same shift with different roles") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        r <- checkIfNurseIsNotRepeated(shiftSchedule)
      } yield r
    result.fold(_ => false, _ => true)
  )
  
  def checkIfNurseIsNotRepeated(shiftSchedule: ShiftSchedule) : Result[Boolean] =
    shiftSchedule.shiftDays.foldLeft[Result[Boolean]](Right(true))(
      (result1, days) =>
        days.shifts.foldLeft[Result[Boolean]](Right(true))(
          (result2, shifts) => 
            if (shifts.nurses.map(n => n.name).toSet.size != shifts.nurses.size) Left(DuplicateNurses) else result2
      )
    )

  property("The shift schedule must schedule all the requirements for each day") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        r <- checkIfRequirementsAreMet(shiftSchedule, info)
      } yield r
    result.fold(_ => false, _ => true)
  )
  
  def checkIfRequirementsAreMet(shiftSchedule: ShiftSchedule, info: ScheduleInfo) : Result[Boolean] =
    shiftSchedule.shiftDays.foldLeft[Result[Boolean]](Right(true))(
      (result1, days) =>
        days.shifts.foldLeft[Result[Boolean]](Right(true))(
          (result2, shifts) => 
            val requirements = info.schedulingPeriods.filter(p => p.id.toString == shifts.id.toString).head.nurseRequirement
            requirements.foldLeft[Result[Boolean]](Right(true))(
              (result3, req) => 
                if(shifts.nurses.filter(n => n.role.toString == req.role.toString).size != req.number) Left(DuplicateNurses) else result2
            )
        )
    )

  property("Max shifts per day constraint must be respected") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        res <- checkIfMaxShiftPerDayConstraintIsRespected(shiftSchedule, info)
      } yield res
    result.fold(_ => false, _ => true)
  )

  def checkIfMaxShiftPerDayConstraintIsRespected(shiftSchedule: ShiftSchedule, info: ScheduleInfo) : Result[Boolean] =
    shiftSchedule.shiftDays.foldLeft[Result[Boolean]](Right(true)) {
      (res, day) =>
        val nursesSet = day.shifts.map(s => s.nurses).flatten
        val nursesList = day.shifts.toList.map(s => s.nurses.toList).flatten

        nursesSet.foldLeft[Result[Boolean]](Right(true)) {
          (res2, nurse) =>
            val count = nursesList.map(n => n.name.toString == nurse.name.toString).size
            if(MaxShiftsPerDay.compareMaxShiftsPerDay2(count, info.constraints.maxShiftsPerDay)) Left(InvalidMaxShiftsPerDay("Max Shifts per day constraint is not respected.")) else res2
        }
    }


  property("Shift Schedule has the right amount of days") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        res <- checkIfShiftScheduleHasRightAmountOfDays(shiftSchedule)
      } yield res
    result.fold(_ => false, _ => true)
  )

  def checkIfShiftScheduleHasRightAmountOfDays(shiftSchedule: ShiftSchedule) : Result[Boolean] =
    val distinctDaysIds = shiftSchedule.shiftDays.foldLeft[Set[String]](Set()){
      (acc, shiftDay) =>
        if(acc.contains(shiftDay.id.toString)) acc else acc ++ Set(shiftDay.id.toString)
    }
    if (distinctDaysIds.size != 7) Left(InvalidDaysQuantity) else Right(true)

  property("Unknown roles must always throw a domain error") = forAll(genFaultyScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- checkForUnknownRole(getShiftSchedule(info))
      } yield shiftSchedule
    result.fold(_ => false, _ => true)
  )

  def checkForUnknownRole(shiftSchedule: Result[ShiftSchedule]) : Result[Boolean] =
    shiftSchedule match
      case Left(value) => Right(true)
      case Right(value) => Left(UnknownRolesNotDetected)

  //MinRestDaysPerWeek Check -> Uncomment in MileStone3
  property("Min rest days per week must be respected") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        res <- checkIfMinRestDaysPerWeekConstraintIsRespected(shiftSchedule, info)
      } yield res
    result.fold(_ => false, _ => true)
  )

  def checkIfMinRestDaysPerWeekConstraintIsRespected(shiftSchedule: ShiftSchedule, info: ScheduleInfo) : Result[Boolean] =
    shiftSchedule.shiftDays.foldLeft[Result[Boolean]](Right(true)) {
      (res, day) =>
        val nursesSet = day.shifts.map(s => s.nurses).flatten
        nursesSet.foldLeft[Result[Boolean]](Right(true)) {
          (res2, nurse) =>
            val count = shiftSchedule.shiftDays.count(shiftDay => shiftDay.shifts.exists(shift => shift.nurses.exists(nur => nur.name == nurse.name)))
            if ((count + MinRestDaysPerWeek.toInt(info.constraints.minRestDaysPerWeek)) <= 7) res2 else Left(InvalidMinRestDays("InvalidMinRestDays"))
        }
    }


  property("Repetition of the algorithm must result in the same schedule") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule1 <- getShiftSchedule(info)
        shiftSchedule2 <- getShiftSchedule(info)
        res <- areSchedulesEquals(shiftSchedule1, shiftSchedule2)
      } yield res
    result.fold(_ => false, _ => true)
  )

  def areSchedulesEquals(shiftSchedule1: ShiftSchedule, shiftSchedule2: ShiftSchedule) : Result[Boolean] =
    val compare = shiftSchedule1 == shiftSchedule2
    compare match
      case true => Right(true)
      case false => Left(UnknownRolesNotDetected)
