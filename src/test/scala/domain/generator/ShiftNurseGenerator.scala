package domain.generator

import scala.language.adhocExtensions
import org.scalacheck.*
import SimpleGenerator.*
import domain.shift.ShiftNurse

object ShiftNurseGenerator extends Properties("ShiftNurses"):
  def genShiftNurse : Gen[ShiftNurse] =
    for {
      name <- genNurseName
      role <- genRole
    } yield ShiftNurse(name, role)
  
  def genListOfShiftNurses : Gen[List[ShiftNurse]] =
    for {
      n <- Gen.chooseNum(1,6)
      lp = (1 to n).map(i => genShiftNurse).toList
      list <- Gen.sequence[List[ShiftNurse], ShiftNurse](lp)
    } yield list

