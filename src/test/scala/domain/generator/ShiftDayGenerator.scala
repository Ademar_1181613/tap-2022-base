package domain.generator

import scala.language.adhocExtensions
import org.scalacheck.*
import org.scalacheck.Prop.forAll
import SimpleGenerator.*
import domain.shift.Shift
import domain.shift.ShiftDay
import ShiftNurseGenerator.*
import ShiftGenerator.*

object ShiftDayGenerator extends Properties("ShiftDays"):

  def genShiftDay(ls: List[Shift]) : Gen[ShiftDay] =
    for {
      id <- genDayNumber
      list <- genShiftsForDay(ls)
    } yield ShiftDay(id, list.toSet)

  def genShiftsForDay(ls: List[Shift]) : Gen[List[Shift]] =
    for {
      n <- Gen.chooseNum(1, 10)
      list <- Gen.listOfN(n, Gen.oneOf(ls))
    } yield list.toList

  def genListOfShiftDays(ls: List[Shift]) : Gen[List[ShiftDay]] =
    val lt = (1 to 7).map(i => genShiftDay(ls)).toList
    Gen.sequence[List[ShiftDay], ShiftDay](lt)
    
  property("Validate generator genShiftDay") = forAll(genListOfShiftNurses)( lsn =>
    forAll(genListOfShifts(lsn))( ls =>
      forAll(genShiftDay(ls))( shiftDay =>
        shiftDay.shifts.forall(s => ls.contains(s))
      )
    )
  )


