package domain.generator

import scala.language.adhocExtensions
import org.scalacheck.*
import org.scalacheck.Prop.forAll
import domain.Simple.{MaxShiftsPerDay, MinRestDaysPerWeek, *}

object SimpleGenerator extends Properties("Simple"):

  def genStringId: Gen[StringId]=
    for{
      n <- Gen.chooseNum(2,10)
      cl <- Gen.listOfN(n, Gen.alphaChar)
      id <- StringId.from(cl.mkString).fold(_ => Gen.fail, id => Gen.const(id))
    }yield id

  def genRole: Gen[Role]=
    for{
      cl <- Gen.oneOf("A", "B", "C")
      role <- Role.from(cl).fold(_ => Gen.fail, role => Gen.const(role))
    }yield role

  def genBadRole: Gen[Role]=
    for{
      cl <- Gen.oneOf("D", "E", "F")
      role <- Role.from(cl).fold(_ => Gen.fail, role => Gen.const(role))
    }yield role

  def genDayNumber: Gen[DayNumber]=
    for{
      value <- Gen.chooseNum(1,7)
      dayNumber <- DayNumber.from(value).fold(_ => Gen.fail, number => Gen.const(number))
    }yield dayNumber

  def genNurseName: Gen[NurseName]=
    for{
      n <- Gen.chooseNum(2,10)
      cl <- Gen.listOfN(n, Gen.alphaChar)
      nurseName <- NurseName.from(cl.mkString).fold(_ => Gen.fail, name => Gen.const(name))
    }yield nurseName

  def genNurseSeniority: Gen[NurseSeniority]=
    for{
      value <- Gen.chooseNum(1,5)
      nurseSeniority <- NurseSeniority.from(value).fold(_ => Gen.fail, seniority => Gen.const(seniority))
    }yield nurseSeniority

  def genPreferenceValue: Gen[PreferenceValue]=
    for{
      value <- Gen.chooseNum(-2,2)
      preferenceValue <- PreferenceValue.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    }yield preferenceValue

  def genMaxShiftsPerDay: Gen[MaxShiftsPerDay]=
    for{
      value <- Gen.chooseNum(15,25)
      maxShiftsPerDay <- MaxShiftsPerDay.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    }yield maxShiftsPerDay

  def genMinRestDaysPerWeek: Gen[MinRestDaysPerWeek]=
    for{
      value <- Gen.chooseNum(3,7)
      minRestDaysPerWeek <- MinRestDaysPerWeek.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    }yield minRestDaysPerWeek

  