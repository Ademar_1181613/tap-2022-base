package domain.generator

import scala.language.adhocExtensions
import org.scalacheck.*
import org.scalacheck.Prop.forAll
import SimpleGenerator.*
import domain.schedulingperiod.SchedulingPeriod
import domain.schedulingperiod.Time
import domain.schedulingperiod.NurseRequirement
import domain.nurse.Nurse
import TimeGenerator.*
import NurseRequirementGenerator.*
import NurseGenerator.*

object SchedulingPeriodGenerator extends Properties("SchedulingPeriods"):

  def genSchedulingPeriod(ln: List[Nurse]) : Gen[SchedulingPeriod] =
    for {
      id <- genStringId
      t1 <- genTime
      t2 <- genTime
      lsn <- genListOfNurseRequirements(ln)
    } yield SchedulingPeriod(id, t1, t2, lsn.toSet)

  def genListOfSchedulingPeriods(ln: List[Nurse]) : Gen[List[SchedulingPeriod]] =
    for {
      n <- Gen.chooseNum(1,3)
      lt = (1 to n).map(i => genSchedulingPeriod(ln)).toList
      list <- Gen.sequence[List[SchedulingPeriod], SchedulingPeriod](lt)
    } yield list

  def genBadSchedulingPeriod : Gen[SchedulingPeriod] =
    for {
      id <- genStringId
      t1 <- genTime
      t2 <- genTime
      lsn <- genBadListOfNurseRequirements
    } yield SchedulingPeriod(id, t1, t2, lsn.toSet)

  def genBadListOfSchedulingPeriods: Gen[List[SchedulingPeriod]] =
    for {
      n <- Gen.chooseNum(1,3)
      lt = (1 to n).map(i => genBadSchedulingPeriod).toList
      list <- Gen.sequence[List[SchedulingPeriod], SchedulingPeriod](lt)
    } yield list

  // property("Validate generator genSchedulingPeriod") = forAll(genListOfNurses)( ln =>
  //   forAll(genListOfNurseRequirements(ln))( lsn =>
  //     forAll(genSchedulingPeriod(lsn))( sp =>
  //       sp.nurseRequirement.forall(n => lsn.contains(n))
  //     )
  //   )
  // )


  