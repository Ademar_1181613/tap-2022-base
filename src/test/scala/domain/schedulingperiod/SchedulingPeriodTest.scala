package domain.schedulingperiod

import domain.DomainError
import domain.Simple.*
import org.scalatest.funsuite.AnyFunSuite

import java.text.SimpleDateFormat

class SchedulingPeriodTest extends AnyFunSuite:

  test("SchedulingPeriod startTime must be before endTime"){
    val result =
      for{
        role <- Role.from("A")
        stringId <- StringId.from("n1")
        nr <- NurseRequirement.from(role,2)
        startTime <- Time.from("12:30:00")
        endTime <- Time.from("10:00:00")
      }yield SchedulingPeriod.from(stringId,startTime,endTime,Set(nr))
    assert(result.fold(error => error == DomainError.InvalidPeriod("Start time must be before End time"), _ => true))
  }

  test("SchedulingPeriod must have at least one NurseRequirement"){
    val nurseRequirements : Set[NurseRequirement] = Set()
    val result =
      for{
        stringId <- StringId.from("n1")
        startTime <- Time.from("12:30:00")
        endTime <- Time.from("14:00:00")
      }yield SchedulingPeriod.from(stringId,startTime,endTime,nurseRequirements)
    assert(result.fold(error => error == DomainError.NoNurseRequirementsError("SchedulingPeriod must have at least 1 NurseRequirement"), _ => true))
  }

  test("SchedulingPeriod is created"){
    val result =
      for{
        role <- Role.from("A")
        stringId <- StringId.from("n1")
        nr <- NurseRequirement.from(role,2)
        startTime <- Time.from("12:30:00")
        endTime <- Time.from("14:00:00")
      }yield SchedulingPeriod.from(stringId,startTime,endTime,Set(nr))
    assert(result.fold(_ => false, _ => true))
  }


