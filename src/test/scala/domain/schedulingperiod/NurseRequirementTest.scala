package domain.schedulingperiod

import domain.DomainError
import org.scalatest.funsuite.AnyFunSuite
import domain.Simple.*

class NurseRequirementTest extends AnyFunSuite:
  

  test("NurseRequirement number must be greater than 0"){
    val result =
      for{
        role <- Role.from("A")
      }yield NurseRequirement.from(role, 0)
    assert(result.fold(error => error == DomainError.InvalidNumber("NurseRequirement's number must be greater than 0"), _ => true))
  }

  test("NurseRequirement is created"){
    val result =
      for{
        role <- Role.from("A")
      }yield NurseRequirement.from(role, 3)
    assert(result.fold(_ => false, _ => true))
  }
