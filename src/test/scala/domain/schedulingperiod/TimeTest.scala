package domain.schedulingperiod

import domain.DomainError
import org.scalatest.funsuite.AnyFunSuite
import domain.Simple.*
import domain.schedulingperiod.Time.*
import scala.language.adhocExtensions
import org.scalacheck.*

class TimeTest extends AnyFunSuite:

  test("Time is created"){
    val result = Time.from(13, 22, 43)
    assert(result.fold(_ => false, _ => true))
  }

  test("Time is created (via String)"){
    val str = "16:00:12"
    val result = Time.from(str)
    assert(result.fold(_ => false, _ => true))
  }

  test("Time Hour is invalid (negative) 1"){    
    val result = Time.from(-16, 22, 43)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Hour value is invalid (negative or more than 23)"), _ => false))
  }

  test("Time Hour is invalid (negative) 2"){  
    val str = "-11:00:12"
    val result = Time.from(str)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Hour value is invalid (negative or more than 23)"), _ => false))
  }

  test("Time Hour is invalid (too high) 1"){    
    val result = Time.from(33, 22, 43)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Hour value is invalid (negative or more than 23)"), _ => false))
  }

  test("Time Hour is invalid (too high) 2"){  
    val str = "56:00:12"
    val result = Time.from(str)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Hour value is invalid (negative or more than 23)"), _ => false))
  }


  test("Time Minute is invalid (negative) 1"){    
    val result = Time.from(16, -22, 43)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Minute value is invalid (negative or more than 59)"), _ => false))
  }

  test("Time Minute is invalid (negative) 2"){  
    val str = "11:-1:12"
    val result = Time.from(str)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Minute value is invalid (negative or more than 59)"), _ => false))
  }

  test("Time Minute is invalid (too high) 1"){    
    val result = Time.from(23, 221, 43)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Minute value is invalid (negative or more than 59)"), _ => false))
  }

  test("Minute Minute is invalid (too high) 2"){  
    val str = "16:200:12"
    val result = Time.from(str)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Minute value is invalid (negative or more than 59)"), _ => false))
  }


  test("Time Second is invalid (negative) 1"){    
    val result = Time.from(16, 22, -43)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Second value is invalid (negative or more than 59)"), _ => false))
  }

  test("Time Second is invalid (negative) 2"){  
    val str = "11:00:-12"
    val result = Time.from(str)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Second value is invalid (negative or more than 59)"), _ => false))
  }

  test("Time Second is invalid (too high) 1"){    
    val result = Time.from(16, 22, 93)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Second value is invalid (negative or more than 59)"), _ => false))
  }

  test("Time Second is invalid (too high) 2"){  
    val str = "11:00:82"
    val result = Time.from(str)
    assert(result.fold(error => error == DomainError.InvalidValue("Time: Second value is invalid (negative or more than 59)"), _ => false))
  }

  test("Time Comparison Test 1 (Equal)"){  
    val result = 
      for{
        time1 <- Time.from("11:00:52")
        time2 <- Time.from("11:00:52")
      }yield Time.compare(time1, time2)
    assert(result.fold(_=>false, _=> result) == Right(0))
  }

  test("Time Comparison Test 2 (T1 Bigger)"){  
    val result = 
      for{
        time1 <- Time.from("16:00:52")
        time2 <- Time.from("11:00:52")
      }yield Time.compare(time1, time2)
    assert(result.fold(_=>false, _=> result) == Right(1))
  }

  test("Time Comparison Test 3 (T1 Bigger)"){  
    val result = 
      for{
        time1 <- Time.from("11:20:52")
        time2 <- Time.from("11:00:12")
      }yield Time.compare(time1, time2)
    assert(result.fold(_=>false, _=> result) == Right(1))
  }


  test("Time Comparison Test 4 (T1 Bigger)"){  
    val result = 
      for{
        time1 <- Time.from("11:00:53")
        time2 <- Time.from("11:00:52")
      }yield Time.compare(time1, time2)
    assert(result.fold(_=>false, _=> result) == Right(1))
  }



  test("Time Comparison Test 5 (T2 Bigger)"){  
    val result = 
      for{
        time1 <- Time.from("11:50:52")
        time2 <- Time.from("14:00:52")
      }yield Time.compare(time1, time2)
    assert(result.fold(_=>false, _=> result) == Right(-1))
  }


  test("Time Comparison Test 6 (T2 Bigger)"){  
    val result = 
      for{
        time1 <- Time.from("11:00:58")
        time2 <- Time.from("11:30:52")
      }yield Time.compare(time1, time2)
    assert(result.fold(_=>false, _=> result) == Right(-1))
  }

  test("Time Comparison Test 7 (T2 Bigger)"){  
    val result = 
      for{
        time1 <- Time.from("11:00:52")
        time2 <- Time.from("11:00:59")
      }yield Time.compare(time1, time2)
    assert(result.fold(_=>false, _=> result) == Right(-1))
  }
  

