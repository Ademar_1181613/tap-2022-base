package domain.shift

import domain.DomainError
import org.scalatest.funsuite.AnyFunSuite
import domain.Simple.*

class ShiftDayTest extends AnyFunSuite:

  // test("Shift Day set of shifts is empty"){
  //   val result = 
  //     for{
  //       shiftDay <- DayNumber.from(5)
  //     }yield ShiftDay.from(shiftDay, Set[domain.shift.Shift]())
  //   assert(result.fold(error => error == DomainError.NoShiftsError("No shifts present in ShiftDay"), _ => true))
  // }
  
  test("Shift Day is created"){
    val result = 
      for{
        shiftDay <- DayNumber.from(5)
        role <- Role.from("role")
        id1 <- StringId.from("morning")
        id2 <- StringId.from("afternoon")
        name1 <- NurseName.from("Ana")
        name2 <- NurseName.from("Maria")
        n1 <- ShiftNurse.from(name1, role)
        n2 <- ShiftNurse.from(name2, role)
        s1 <- Shift.from(id1, Set(n1,n2), 2)
        s2 <- Shift.from(id2, Set(n1,n2), 2)
      }yield ShiftDay.from(shiftDay, Set(s1,s2))
    assert(result.fold(_ => false, _ => true))
  }
