package domain.shift

import domain.DomainError
import domain.Simple.*
import org.scalatest.funsuite.AnyFunSuite

class ShiftScheduleTest extends AnyFunSuite:

  test("Shift Schedule days count is below 7"){
    val result = 
      for{
        id1 <- StringId.from("morning")
        id2 <- StringId.from("afternoon")
        id3 <- StringId.from("evening")
        id4 <- StringId.from("night")
        role <- Role.from("role")
        name1 <- NurseName.from("Ana")
        name2 <- NurseName.from("Maria")
        n1 <- ShiftNurse.from(name1, role)
        n2 <- ShiftNurse.from(name2, role)
        s1 <- Shift.from(id1, Set(n1,n2),2)
        s2 <- Shift.from(id2, Set(n1,n2),2)
        s3 <- Shift.from(id3, Set(n1,n2),2)
        s4 <- Shift.from(id4, Set(n1,n2),2)
        day1 <- DayNumber.from(1)
        day2 <- DayNumber.from(2)
        day3 <- DayNumber.from(3)
        day4 <- DayNumber.from(4)
        day5 <- DayNumber.from(5)
        day6 <- DayNumber.from(6)
        day7 <- DayNumber.from(7)
        shiftDay1 <- ShiftDay.from(day1, Set(s1,s2))
        shiftDay2 <- ShiftDay.from(day2, Set(s1,s2))
        shiftDay3 <- ShiftDay.from(day3, Set(s1,s2))
        shiftDay4 <- ShiftDay.from(day4, Set(s1,s2))
        shiftDay5 <- ShiftDay.from(day5, Set(s1,s2))
        shiftDay6 <- ShiftDay.from(day6, Set(s1,s2))
        shiftDay7 <- ShiftDay.from(day7, Set(s1,s2))
        shiftDay8 <- ShiftDay.from(day6, Set(s3,s4))
        shiftDay9 <- ShiftDay.from(day7, Set(s3,s4))
      }yield ShiftSchedule.from(Set(shiftDay1, shiftDay2, shiftDay3, shiftDay4, shiftDay5))
    assert(result.fold(error => error == DomainError.MissingDaysError("Incorrect number of Days in Shift Schedule"), _ => true))
  }
  

  test("Shift Schedule days count is above 7"){
    val result = 
      for{
        id1 <- StringId.from("morning")
        id2 <- StringId.from("afternoon")
        id3 <- StringId.from("evening")
        id4 <- StringId.from("night")
        role <- Role.from("role")
        name1 <- NurseName.from("Ana")
        name2 <- NurseName.from("Maria")
        n1 <- ShiftNurse.from(name1, role)
        n2 <- ShiftNurse.from(name2, role)
        s1 <- Shift.from(id1, Set(n1,n2),2)
        s2 <- Shift.from(id2, Set(n1,n2),2)
        s3 <- Shift.from(id3, Set(n1,n2),2)
        s4 <- Shift.from(id4, Set(n1,n2),2)
        day1 <- DayNumber.from(1)
        day2 <- DayNumber.from(2)
        day3 <- DayNumber.from(3)
        day4 <- DayNumber.from(4)
        day5 <- DayNumber.from(5)
        day6 <- DayNumber.from(6)
        day7 <- DayNumber.from(7)
        shiftDay1 <- ShiftDay.from(day1, Set(s1,s2))
        shiftDay2 <- ShiftDay.from(day2, Set(s1,s2))
        shiftDay3 <- ShiftDay.from(day3, Set(s1,s2))
        shiftDay4 <- ShiftDay.from(day4, Set(s1,s2))
        shiftDay5 <- ShiftDay.from(day5, Set(s1,s2))
        shiftDay6 <- ShiftDay.from(day6, Set(s1,s2))
        shiftDay7 <- ShiftDay.from(day7, Set(s1,s2))
        shiftDay8 <- ShiftDay.from(day6, Set(s3,s4))
        shiftDay9 <- ShiftDay.from(day7, Set(s3,s4))
      }yield ShiftSchedule.from(Set(shiftDay1, shiftDay2, shiftDay3, shiftDay4, shiftDay5, shiftDay6, shiftDay7, shiftDay8, shiftDay9))
    assert(result.fold(error => error == DomainError.MissingDaysError("Incorrect number of Days in Shift Schedule"), _ => true))
  }
  

  test("Shift Schedule is created"){
    val result =
      for{
      id1 <- StringId.from("morning")
      id2 <- StringId.from("afternoon")
      id3 <- StringId.from("evening")
      id4 <- StringId.from("night")
      role <- Role.from("role")
      name1 <- NurseName.from("Ana")
      name2 <- NurseName.from("Maria")
      n1 <- ShiftNurse.from(name1, role)
      n2 <- ShiftNurse.from(name2, role)
      s1 <- Shift.from(id1, Set(n1,n2),2)
      s2 <- Shift.from(id2, Set(n1,n2),2)
      s3 <- Shift.from(id3, Set(n1,n2),2)
      s4 <- Shift.from(id4, Set(n1,n2),2)
      day1 <- DayNumber.from(1)
      day2 <- DayNumber.from(2)
      day3 <- DayNumber.from(3)
      day4 <- DayNumber.from(4)
      day5 <- DayNumber.from(5)
      day6 <- DayNumber.from(6)
      day7 <- DayNumber.from(7)
      shiftDay1 <- ShiftDay.from(day1, Set(s1,s2))
      shiftDay2 <- ShiftDay.from(day2, Set(s1,s2))
      shiftDay3 <- ShiftDay.from(day3, Set(s1,s2))
      shiftDay4 <- ShiftDay.from(day4, Set(s1,s2))
      shiftDay5 <- ShiftDay.from(day5, Set(s1,s2))
      shiftDay6 <- ShiftDay.from(day6, Set(s1,s2))
      shiftDay7 <- ShiftDay.from(day7, Set(s1,s2))
      shiftDay8 <- ShiftDay.from(day6, Set(s3,s4))
      shiftDay9 <- ShiftDay.from(day7, Set(s3,s4))
    }yield ShiftSchedule.from(Set(shiftDay1, shiftDay2, shiftDay3, shiftDay4, shiftDay5, shiftDay6, shiftDay7))
    assert(result.fold(_ => false, _ => true))
  }
