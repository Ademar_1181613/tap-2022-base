package domain.shift

import domain.DomainError
import org.scalatest.funsuite.AnyFunSuite
import domain.Simple.*
import scala.language.adhocExtensions
import org.scalacheck.*

class ShiftNurseTest extends AnyFunSuite:

  test("Shift Nurse name is empty"){
    val result = 
      for{
        name <- NurseName.from("")
        role <- Role.from("role")
      } yield ShiftNurse.from(name,  role)
    assert(result.fold(error => error == DomainError.EmptyName, _ => true))
  }

  test("Shift Nurse name is blank"){
    val result = 
      for{
        name <- NurseName.from("        ")
        role <- Role.from("role")
      } yield ShiftNurse.from(name,  role)
    assert(result.fold(error => error == DomainError.EmptyName, _ => true))
  }

  test("Shift Nurse is created"){
    val result = 
      for{        
        name <- NurseName.from("Ana")
        role <- Role.from("role")
      } yield ShiftNurse.from(name,  role)
    assert(result.fold(_ => false, _ => true))
  }

