package domain.shift

import domain.DomainError
import domain.Simple.*
import org.scalatest.funsuite.AnyFunSuite

class ShiftTest extends AnyFunSuite:

  test("Shift set of nurses is empty"){
    val result =
      for{
        id <- StringId.from("morning")
        role <- Role.from("role")
      } yield Shift.from(id, Set[domain.shift.ShiftNurse](), 2)
    assert(result.fold(error => error == DomainError.NotEnoughNurses, _ => true))
  }
  
  test("Shift is created"){
    val result =
      for{
        id <- StringId.from("morning")
        role <- Role.from("role")
        name1 <- NurseName.from("Ana")
        name2 <- NurseName.from("Maria")
        n1 <- ShiftNurse.from(name1, role)
        n2 <- ShiftNurse.from(name2, role)
      } yield Shift.from(id, Set(n1,n2), 2)
    assert(result.fold(_ => false, _ => true))
  }
