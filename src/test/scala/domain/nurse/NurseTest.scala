package domain.nurse

import domain.DomainError
import org.scalatest.funsuite.AnyFunSuite
import domain.Simple.*

class NurseTest extends AnyFunSuite:
  
  test("Nurse Seniority without roles"){
    val result = 
      for{
        nurseName <- NurseName.from("Ana")
        nurseSeniority <- NurseSeniority.from(3)
        roleA <- Role.from("A")
        roleB <- Role.from("B")
      } yield Nurse.from(nurseName, nurseSeniority,Set[Role]())
    assert(result.fold(error => error == DomainError.NoRolesError("The nurse must have at least 1 role"), _ => true))
  }

  test("Nurse is created"){
    val result = 
      for{
        nurseName <- NurseName.from("Ana")
        nurseSeniority <- NurseSeniority.from(3)
        roleA <- Role.from("A")
        roleB <- Role.from("B")
      } yield Nurse.from(nurseName, nurseSeniority,Set(roleA, roleB))
    assert(result.fold(_ => false, _ => true))
  }
