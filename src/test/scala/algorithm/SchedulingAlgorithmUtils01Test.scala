package algorithm

import domain.DomainError
import domain.Simple.*
import domain.constraint.*
import domain.shift.*
import algorithm.*
import org.scalatest.funsuite.AnyFunSuite

class SchedulingAlgorithmUtils01Test extends AnyFunSuite:

  test("Max Shifts Per day reached"){
    val res =
      for {
        role <- Role.from("role")
        id1 <- StringId.from("morning")
        id2 <- StringId.from("afternoon")
        id3 <- StringId.from("evening")
        m1 <- MinRestDaysPerWeek.from(2)
        m2 <- MaxShiftsPerDay.from(3)
        name1 <- NurseName.from("Ana")
        name2 <- NurseName.from("Maria")
        n1 <- ShiftNurse.from(name1, role)
        n2 <- ShiftNurse.from(name2, role)
        shift1 <- Shift.from(id1, Set(n1, n2), 2)
        shift2 <- Shift.from(id2, Set(n1, n2), 2)
        shift3 <- Shift.from(id3, Set(n1, n2), 2)
      }
      yield SchedulingAlgorithmUtils01.maxShiftsPerDayValidation(Constraint.from(m1,m2),  Set(shift1,shift2,shift3), name2)
    assert(res.fold(_=>false, _=> res) == Right(false))
  }

  test("Max Shifts Per day not reached"){
    val res = 
      for {
        role <- Role.from("role")
        id1 <- StringId.from("morning")
        id2 <- StringId.from("afternoon")
        id3 <- StringId.from("evening")
        m1 <- MinRestDaysPerWeek.from(2)
        m2 <- MaxShiftsPerDay.from(3)
        name1 <- NurseName.from("Ana")
        name2 <- NurseName.from("Maria")
        name3 <- NurseName.from("Joana")
        n1 <- ShiftNurse.from(name1, role)
        n2 <- ShiftNurse.from(name2, role)
        n3 <- ShiftNurse.from(name3, role)
        shift1 <- Shift.from(id1, Set(n1, n2), 2)
        shift2 <- Shift.from(id2, Set(n1, n2), 2)
        shift3 <- Shift.from(id3, Set(n1, n3), 2)
      }
      yield SchedulingAlgorithmUtils01.maxShiftsPerDayValidation(Constraint(m1,m2),  Set(shift1,shift2,shift3), name2)
    assert(res.fold(_=>false, _=> res) == Right(true))
  }

  test("Shift: nurse not yet in it"){
    val res = 
      for {        
        role <- Role.from("role")
        name1 <- NurseName.from("Ana")
        name2 <- NurseName.from("Maria")
        name3 <- NurseName.from("Joana")
        n1 <- ShiftNurse.from(name1, role)
        n2 <- ShiftNurse.from(name2, role)
      }
      yield SchedulingAlgorithmUtils01.nurseAlreadyInShiftValidation(Set(n1, n2), name3)
    assert(res.fold(_=>false, _=> res) == Right(false))
  }
  
  test("Shift: nurse is already in it"){
    val res = 
      for {
        role <- Role.from("role")
        name1 <- NurseName.from("Ana")
        name2 <- NurseName.from("Maria")
        n1 <- ShiftNurse.from(name1, role)
        n2 <- ShiftNurse.from(name2, role)
      }
      yield SchedulingAlgorithmUtils01.nurseAlreadyInShiftValidation(Set(n1, n2), name1)
    assert(res.fold(_=>false, _=> res) == Right(true))
  }


  test("Shift number of nurses is not met"){
    val res = 
      for {
        role <- Role.from("role")
        name1 <- NurseName.from("Ana")
        name2 <- NurseName.from("Maria")
        n1 <- ShiftNurse.from(name1, role)
        n2 <- ShiftNurse.from(name2, role)
      }
      yield SchedulingAlgorithmUtils01.nurseRequirementMet(Set(n1, n2), role, 4)
    assert(res.fold(_=>false, _=> res) == Right(false))
  }
  
  test("Shift number of nurses is met"){
    val res = 
      for {
        role <- Role.from("role")
        name1 <- NurseName.from("Ana")
        name2 <- NurseName.from("Maria")
        n1 <- ShiftNurse.from(name1, role)
        n2 <- ShiftNurse.from(name2, role)
      }
      yield SchedulingAlgorithmUtils01.nurseRequirementMet(Set(n1, n2), role, 2)
    assert(res.fold(_=>false, _=> res) == Right(true))
  }
  
