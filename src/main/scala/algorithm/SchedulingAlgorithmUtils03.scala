package algorithm

import domain.{Result, ScheduleInfo}
import domain.shift.*
import domain.constraint.*
import domain.nurse.*
import domain.preference.*
import domain.Simple.*
import domain.schedulingperiod.{NurseRequirement, SchedulingPeriod}
import domain.ScheduleInfo.*
import domain.DomainError
import scala.util.Sorting

object SchedulingAlgorithmUtils03:

  //Constraint Verification Start
  def maxShiftsPerDayValidation(constraint: Constraint, shifts: Set[Shift], nurseName: NurseName): Boolean =
    val count: Int = shifts.count(shift => shift.nurses.exists(nurse => nurse.name == nurseName))
    MaxShiftsPerDay.compareMaxShiftsPerDay(count, constraint.maxShiftsPerDay)


  def minRestDaysPerWeekValidation(constraint: Constraint, days: Set[ShiftDay], shifts: Set[Shift], nurseName: NurseName): Boolean =
    val count1 = 
      days.foldLeft[Int](0) {
        (acc, day) =>
          val nursesList = day.shifts.count(shift => shift.nurses.exists(nurse => nurse.name == nurseName))
          if(nursesList>0) acc+1 else acc
      }

    MinRestDaysPerWeek.compareMinRestDaysPerWeek(7-count1, constraint.minRestDaysPerWeek)


  def nurseAlreadyInShiftValidation(shiftNurses: Set[ShiftNurse], nurseName: NurseName): Boolean =
    shiftNurses.exists(nurse => nurse.name == nurseName)

  def nurseRequirementMet(shiftNurses:  Set[ShiftNurse], nurseRole: Role, number: Int): Boolean =
    val count: Int = shiftNurses.count(nurse => nurse.role == nurseRole)
    if(count == number) true else false


  def checkForUnknownRoles(si: ScheduleInfo): Set[String] =
    val rolesNurses = si.nurses.map(n => n.roles).flatten.map(ro => ro.toString)    
    val rolesPeriods = si.schedulingPeriods.map(sp => sp.nurseRequirement).flatten.map(rq => rq.role.toString)
    rolesPeriods.diff(rolesNurses)

  //Constraint Verification End


  // Preference Management

  def getDayPreference(dayPreferenceList: Set[DayPreference], nurse: Nurse, day: DayNumber): Int =
    val list = dayPreferenceList.filter(pref => DayNumber.toInt(pref.DayValue) == DayNumber.toInt(day) && pref.NurseId.toString() == nurse.name.toString())
    if(list.size > 0) 
      val pref = list.head 
      PreferenceValue.getValue(pref.Value)
    else 
      0

  def getPeriodPreference(periodPreferenceList: Set[PeriodPreference], nurse: Nurse, period: StringId): Int =
    val list = periodPreferenceList.filter(pref => pref.PeriodId == period && pref.NurseId.toString() == nurse.name.toString())
    if(list.size > 0) 
      val pref = list.head 
      PreferenceValue.getValue(pref.Value)
    else 
      0

  def generateNursesOrderedByFactor(nurseList: Set[Nurse], day: DayNumber, period: StringId, dayPreferenceList: Set[DayPreference], periodPreferenceList: Set[PeriodPreference]) : List[Nurse] =
    val tupleList = nurseList.foldLeft[Set[(Nurse, Int, Int)]](Set()) {
      (acc, nurse) =>
        val prefDay = getDayPreference(dayPreferenceList, nurse, day) * NurseSeniority.getValue(nurse.seniority)   
        val prefPeriod = getPeriodPreference(periodPreferenceList, nurse, period) * NurseSeniority.getValue(nurse.seniority)

        acc ++ Set((nurse, prefDay, prefPeriod))
    }
    tupleList.toList.sortBy(u => (-(u._2), -(u._3), u._1.roles.size, u._1.name.toString)).map(_._1).toList

  // Preference Management End



  
  //Scheduling Processes
  
  ////////////////////////
  // LOOK AHEAD VERSION //
  ////////////////////////
  
  def getShiftScheduleV2(si: ScheduleInfo, sDays: Set[ShiftDay], sShifts: Set[Shift], sNurses: Set[ShiftNurse], reqNurses: Set[ShiftNurse]): Result[ShiftSchedule] =
          
    val difference = checkForUnknownRoles(si)

    difference.size!= 0 match
      case true => Left(DomainError.UnknownRequirementRole(difference.head))

      case false =>   
        val dayToStart = sDays.size + 1;
        val periodToStartNumber = sShifts.size;
        val periodToStart = si.schedulingPeriods.toList(periodToStartNumber);
        val reqToStart = findRequirement(periodToStart.nurseRequirement, reqNurses.size);
            
        val dayNumbers = (dayToStart to 7).foldLeft[List[DayNumber]](List[DayNumber]()){
          (acc, n) => 
            val nl: List[DayNumber] = acc :+ DayNumber.from2(n)
            nl
        }


        val daysP = 
          dayNumbers.foldLeft[(Boolean, Set[ShiftDay])](true, sDays) {
            (acc, dayNumber) =>
              if(acc._1)
                val ss = getShifts(dayNumber, dayToStart, sShifts )
                val s = si.schedulingPeriods.foldLeft[(Boolean, Int, Set[Shift])]((true, 0, ss)){
                  (acc2, sp) =>         
                    if(acc2._1)         
                      if((dayNumber same dayToStart) && acc2._2<periodToStartNumber)
                        (acc2._1, acc2._2 + 1, acc2._3)
                      else                    
                        val nursesOrderedByDay = generateNursesOrderedByFactor(si.nurses, dayNumber, sp.id, si.dayPreferences, si.periodPreferences)
                        
                        createShiftsV2(si, reqToStart, sNurses, reqNurses, acc2._3, si.constraints, sp.id, nursesOrderedByDay, sp.nurseRequirement, acc._2) match
                          case Left(value) => 
                            (false, acc2._2 + 1, acc2._3)
                          case Right(value) =>
                            (acc2._1, acc2._2 + 1, (acc2._3 ++ Set(value)))
                    else
                      acc2
                }
                
                ShiftDay.from(dayNumber, s._3, si.schedulingPeriods.size) match
                  case Left(value) => 
                    (false, acc._2)
                  case Right(value) => (acc._1, acc._2 ++ Set(value))
              
              else
                acc
                
          }

        val days = daysP._2

        if(days.size<7 || days.filter(d => d.shifts.size == 0).size > 0)
          println(days)
          Left(DomainError.NotEnoughNurses)
        else
          println(days)
          ShiftSchedule.from(days)


  def getShifts(dn: DayNumber, dts: Int, s: Set[Shift]) =
    if(dn same dts)
      s
    else 
      Set()

  def getNurses(a1: Int, a2: Int, a3: Set[ShiftNurse]) =
    if(a1 == a2)
      a3
    else 
      Set()


  def createShiftsV2(si: ScheduleInfo, reqNumber: Int, sNurses: Set[ShiftNurse], reqNurses: Set[ShiftNurse], shifts: Set[Shift], constraints: Constraint, id: StringId, nurses: List[Nurse], nr: Set[NurseRequirement], days: Set[ShiftDay]): Result[Shift] =

    val sn = nr.foldLeft[(Int, Set[ShiftNurse])]((0, sNurses)){
      (acc, req) =>
        if(acc._1 < reqNumber)
          (acc._1 + 1, acc._2)
        else

          val filteredNurses = nurses.filter(n => n.roles.exists(role => role.toString == req.role.toString))//.toList.sortWith(_.name.toString < _.name.toString)
          
          val nnn = getNurses(acc._1, reqNumber, reqNurses)
          val tmp = filteredNurses.foldLeft[Set[ShiftNurse]](nnn){
            (nnurses, nur) =>
              val nameString = nur.name
              val a1 = maxShiftsPerDayValidation(constraints, shifts, nameString)
              val a2 = minRestDaysPerWeekValidation(constraints, days, shifts, nameString)
              val a3 = !nurseAlreadyInShiftValidation(acc._2++nnurses, nameString)
              val a4 = !nurseRequirementMet(nnurses, req.role, req.number)        

              if(a1 && a2 && a3 && a4)

                lookAhead(si, days, shifts, acc._2, nnurses ++ Set(ShiftNurse(nameString, req.role)), nr, id) match
                  case Left(value) => 
                    nnurses
                  case Right(value) => 
                    nnurses ++ Set(ShiftNurse(nameString, req.role))

              else
                nnurses
            }
          (acc._1 + 1, acc._2 ++ tmp)
      }
      val sumNumber = nr.foldLeft[Int](0){(sum, rr) => sum + rr.number}
      Shift.from(id,sn._2, sumNumber)


  def lookAhead(si: ScheduleInfo, sDays: Set[ShiftDay], sShifts: Set[Shift], sNurses: Set[ShiftNurse], reqNurses: Set[ShiftNurse], nr: Set[NurseRequirement], id: StringId): Result[ShiftSchedule] =
    val nn = nr.foldLeft[Int](0){(sum, rr) => sum + rr.number}

    if(sDays.size == 7)
      println(sDays)
      ShiftSchedule.from(sDays)

    val res = if(sNurses.size + reqNurses.size == nn)
      Shift.from(id,sNurses++reqNurses,nn) match
        case Left(value) => Left(DomainError.NotEnoughNurses)
        case Right(value) =>
          if((sShifts.size + 1) == si.schedulingPeriods.size)
            ShiftDay.from(DayNumber.from2(sDays.size+1), sShifts ++ Set(value), si.schedulingPeriods.size) match
              case Left(value2) => Left(DomainError.NotEnoughNurses)
              case Right(value2) =>                  
                val nDays = sDays ++ Set(value2)
                if(nDays.size == 7)
                  ShiftSchedule.from(nDays)
                else
                  getShiftScheduleV2(si, nDays, Set(), Set(), Set())
          else
            getShiftScheduleV2(si, sDays, sShifts ++ Set(value), Set(), Set()) 
    else
      getShiftScheduleV2(si, sDays, sShifts, sNurses, reqNurses)

    res



  def findRequirement(requirements: Set[NurseRequirement], nurseNumber: Int): Int =
     
    val r = requirements.foldLeft[(Int, Int, Int, NurseRequirement)]((0, 0, 0, null)){
      (acc, r) => 
        val nr = acc._2 + r.number
        if(acc._3 < nurseNumber && nurseNumber <= nr)
          (acc._1 + 1, acc._1 + 1, nr, r)
        else
          (acc._1 + 1, acc._2, nr, acc._4)
    }

    r._2









  /////////////////////////
  //BACKTRACKING VERSION //
  /////////////////////////

  def getShiftSchedule(si: ScheduleInfo): Result[ShiftSchedule] =
    
    val difference = checkForUnknownRoles(si)

    difference.size!= 0 match
      case true => Left(DomainError.UnknownRequirementRole(difference.head))

      case false =>   
        val dayNumbers = List(DayNumber.from2(1), DayNumber.from2(2), DayNumber.from2(3), DayNumber.from2(4), DayNumber.from2(5),DayNumber.from2(6), DayNumber.from2(7))

        val days = 
          dayNumbers.foldLeft[Set[ShiftDay]](Set()) {
            (acc, dayNumber) =>
              val s: Set[Shift] = si.schedulingPeriods.foldLeft[Set[Shift]](Set()){
                (acc2, sp) =>
                  val nursesOrderedByDay = generateNursesOrderedByFactor(si.nurses, dayNumber, sp.id, si.dayPreferences, si.periodPreferences)
                  
                  createShifts(acc2, si.constraints, sp.id, nursesOrderedByDay, sp.nurseRequirement, acc) match
                    case Left(value) => acc2
                    case Right(value) => acc2 ++ Set(value)
              }
              
              ShiftDay.from(dayNumber, s, si.schedulingPeriods.size) match
                case Left(value) => acc
                case Right(value) => acc ++ Set(value)
                
          }

        if(days.size<7 || days.filter(d => d.shifts.size == 0).size > 0)

          val finalTry = retry(si, days)          
          
          if(finalTry.size<7)
            Left(DomainError.NotEnoughNurses)
          else            
            ShiftSchedule.from(finalTry)
        else
          ShiftSchedule.from(days)



  def retry(si: ScheduleInfo, days: Set[ShiftDay]): Set[ShiftDay] =
    
    val dayNumbers = List(DayNumber.from2(1), DayNumber.from2(2), DayNumber.from2(3), DayNumber.from2(4), DayNumber.from2(5),DayNumber.from2(6), DayNumber.from2(7))

    val d = dayNumbers.toList diff days.map(day => day.id).toList  

    if(d.size == 0)
      days
    else

      val neededNursesAndRequirement = findNeededNurses(si, d.head, days)
      val neededNurses = neededNursesAndRequirement._1
      val requirement = neededNursesAndRequirement._2

      val recDays = recreateDay(si, d.head, days.filter(ddd => ddd.id < d.head), requirement) 
        match
          case Right(value) =>      
            val prevDays = days.filter(ddd => ddd.id <  d.head) ++ Set(value)
            val rDays = dayNumbers.filter(day => day >= d.head).toList

            val ndays1 = 
              rDays.foldLeft[Set[ShiftDay]](prevDays) {
                (acc, dayNumber) =>
                  val s: Set[Shift] = si.schedulingPeriods.foldLeft[Set[Shift]](Set()){
                    (acc2, sp) =>
                      val nursesOrderedByDay = generateNursesOrderedByFactor(si.nurses, dayNumber, sp.id, si.dayPreferences, si.periodPreferences)
                      createShifts(acc2, si.constraints, sp.id, nursesOrderedByDay, sp.nurseRequirement, acc) match
                        case Left(value) => acc2
                        case Right(value) => acc2 ++ Set(value)
                  }
                  
                  ShiftDay.from(dayNumber, s, si.schedulingPeriods.size) match
                    case Left(value) => acc
                    case Right(value) => acc ++ Set(value)
                    
              }

            if(ndays1.size==7 && ndays1.filter(d => d.shifts.size == 0).size == 0)
              ndays1
            else   
              days

          case Left(value) => days

      if(recDays != days)
        recDays
      else
        val previousDays = dayNumbers.filter(ddd => DayNumber.toInt(ddd) <= DayNumber.toInt(d.head)).sortWith(DayNumber.toInt(_) > DayNumber.toInt(_))
              
        val fff = previousDays.foldLeft[Set[ShiftDay]](Set()) {
          (accum, previousDay) =>
            
            val newDays = days.filter(ddd => ddd.id < previousDay)
            val remainingDays = dayNumbers.filter(day => day >= previousDay).toList
          
            val finalVersion = neededNurses.foldLeft[Set[ShiftDay]](accum) {
              (ac, nurse) =>    
                                
                val newVersion = remainingDays.foldLeft[Set[ShiftDay]](newDays) {
                  (acc, day) =>
                    val s: Set[Shift] = createDay(si, day, previousDay, acc, nurse)

                    //println("DAY: " + day + " PDAY: " + previousDay + " n: " + nurse + " s: " + s)
                    
                    ShiftDay.from(day, s, si.schedulingPeriods.size) match
                      case Left(value) => 
                        
                        if((day same 6) && (d.head same 7))     
                          val s: Set[Shift] = createDayReverse(si, day, previousDay, acc, nurse)
                            
                          // println("DAY: " + day + " d.head: " + d.head + " s: " + s)
                          ShiftDay.from(day, s, si.schedulingPeriods.size) match
                            case Left(value) => acc
                            case Right(value) => acc ++ Set(value)
                        else
                          acc
                      case Right(value) => acc ++ Set(value)
                            
                }
      
                if(ac.size==0 && newVersion.size==7) 
                  println(newVersion)
                  newVersion
                else                  
                  if(newVersion.size > days.size)
                    if((previousDay same 5) && nurse.name.toString == "n2")
                      val newerVersion = retry(si, newVersion)
                      newerVersion
                    else
                      ac
                  else
                  ac
            }

            if(accum.size==0 && finalVersion.size==7) 
              finalVersion
            else
              accum
        }
        
        fff
    
    
  def findNeededNurses(si: ScheduleInfo, day: DayNumber, days: Set[ShiftDay]): (List[Nurse], StringId) =
    val v = si.schedulingPeriods.foldLeft[(Set[Shift], (List[Nurse], StringId))](Set(), (List(), StringId.from("StringId").right.get)){
        (acc2, sp) =>
          val nursesOrderedByDay = generateNursesOrderedByFactor(si.nurses, day, sp.id, si.dayPreferences, si.periodPreferences)
          createShifts(acc2._1, si.constraints, sp.id, nursesOrderedByDay, sp.nurseRequirement, days) match
            case Left(value) => acc2
            case Right(value) => (acc2._1, (nursesOrderedByDay, sp.id))
      }
    v._2

  def createDay(si: ScheduleInfo, day: DayNumber, revisionDay: DayNumber, days: Set[ShiftDay], nurse:Nurse): Set[Shift] =        
    si.schedulingPeriods.foldLeft[Set[Shift]](Set()){
      (acc2, sp) =>
        val nursesOrderedByDay = generateNursesOrderedByFactor(si.nurses, day, sp.id, si.dayPreferences, si.periodPreferences)
                  
        if(day == revisionDay)
          createShifts(acc2, si.constraints, sp.id, nursesOrderedByDay.filter(n => n.name != nurse.name), sp.nurseRequirement, days) match
            case Left(value) => acc2
            case Right(value) => acc2 ++ Set(value)
        else
          createShifts(acc2, si.constraints, sp.id, nursesOrderedByDay, sp.nurseRequirement, days) match
            case Left(value) => acc2
            case Right(value) => acc2 ++ Set(value)
    }

  def createDayReverse(si: ScheduleInfo, day: DayNumber, revisionDay: DayNumber, days: Set[ShiftDay], nurse:Nurse): Set[Shift] =        
    val nsp = si.schedulingPeriods.toList.reverse.foldLeft[Set[Shift]](Set()){
      (acc2, sp) =>
        val nursesOrderedByDay = generateNursesOrderedByFactor(si.nurses, day, sp.id, si.dayPreferences, si.periodPreferences)
                  
        if(day == revisionDay)
          createShifts(acc2, si.constraints, sp.id, nursesOrderedByDay.filter(n => n.name != nurse.name), sp.nurseRequirement, days) match
            case Left(value) => acc2
            case Right(value) => acc2 ++ Set(value)
        else
          createShifts(acc2, si.constraints, sp.id, nursesOrderedByDay, sp.nurseRequirement, days) match
            case Left(value) => acc2
            case Right(value) => acc2 ++ Set(value)
    }

    si.schedulingPeriods.foldLeft[Set[Shift]](Set()){
      (acc2, sp) =>
        acc2 ++ nsp.filter(n => n.id.toString == sp.id.toString)
    }

  def recreateDay(si: ScheduleInfo, day: DayNumber, days: Set[ShiftDay], problematicShift: StringId): Result[ShiftDay] =
    val per = si.schedulingPeriods.filter(p => p.id == problematicShift).head
        
    val nn = si.schedulingPeriods.toList.reverse.foldLeft[Set[Shift]](Set()){
      (acc2, sp) =>
        val nursesOrderedByDay = generateNursesOrderedByFactor(si.nurses, day, sp.id, si.dayPreferences, si.periodPreferences)
        createShifts(acc2, si.constraints, sp.id, nursesOrderedByDay, sp.nurseRequirement, days) match
          case Left(value) => acc2
          case Right(value) => acc2 ++ Set(value)
    }

    val nnn = si.schedulingPeriods.foldLeft[Set[Shift]](Set()){
      (acc2, sp) =>
        acc2 ++ nn.filter(n => n.id.toString == sp.id.toString)
    }

    ShiftDay.from(day, nnn, si.schedulingPeriods.size)



  def createShifts(shifts: Set[Shift], constraints: Constraint, id: StringId, nurses: List[Nurse], nr: Set[NurseRequirement], days: Set[ShiftDay]): Result[Shift] =

    val sn: Set[ShiftNurse] = nr.foldLeft[Set[ShiftNurse]](Set()){
      (acc, req) =>
        val filteredNurses = nurses.filter(n => n.roles.exists(role => role.toString == req.role.toString))//.toList.sortWith(_.name.toString < _.name.toString)
        val tmp = filteredNurses.foldLeft[Set[ShiftNurse]](Set()){
          (nurses, nur) =>
            val nameString = nur.name
            val a1 = maxShiftsPerDayValidation(constraints, shifts, nameString)
            val a2 = minRestDaysPerWeekValidation(constraints, days, shifts, nameString)
            val a3 = !nurseAlreadyInShiftValidation(acc, nameString)
            val a4 = !nurseRequirementMet(nurses, req.role, req.number)

            //println("N: " + nameString + " A1:" + a1 + "A2:" + a2 + "A3:" + a3 + "A4:" + a4)
            
            if(a1 && a2 && a3 && a4)
              // println(nameString)
              nurses ++ Set(ShiftNurse(nameString, req.role))
            else
              nurses
        }
        acc ++ tmp
    }
    val sumNumber = nr.foldLeft[Int](0){(sum, rr) => sum + rr.number}
    Shift.from(id,sn, sumNumber)

