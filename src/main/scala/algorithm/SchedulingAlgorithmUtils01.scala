package algorithm

import domain.{Result, ScheduleInfo}
import domain.shift.*
import domain.constraint.*
import domain.nurse.*
import domain.preference.*
import domain.Simple.*
import domain.schedulingperiod.{NurseRequirement, SchedulingPeriod}
import domain.ScheduleInfo.*
import domain.DomainError

object SchedulingAlgorithmUtils01:

  //Constraint Verification Start
  def maxShiftsPerDayValidation(constraint: Constraint, shifts: Set[Shift], nurseName: NurseName): Boolean =
    val count: Int = shifts.count(shift => shift.nurses.exists(nurse => nurse.name == nurseName))
    MaxShiftsPerDay.compareMaxShiftsPerDay(count, constraint.maxShiftsPerDay)


  def nurseAlreadyInShiftValidation(shiftNurses: Set[ShiftNurse], nurseName: NurseName): Boolean =
    shiftNurses.exists(nurse => nurse.name == nurseName)


  def nurseRequirementMet(shiftNurses:  Set[ShiftNurse], nurseRole: Role, number: Int): Boolean =
    val count: Int = shiftNurses.count(nurse => nurse.role == nurseRole)
    if(count == number) true else false


  def checkForUnknownRoles(si: ScheduleInfo): Set[String] =
    val rolesNurses = si.nurses.map(n => n.roles).flatten.map(ro => ro.toString)    
    val rolesPeriods = si.schedulingPeriods.map(sp => sp.nurseRequirement).flatten.map(rq => rq.role.toString)
    rolesPeriods.diff(rolesNurses)

  //Constraint Verification End



  
  //Scheduling Processes

  def getShiftSchedule(si: ScheduleInfo): Result[ShiftSchedule] =
    
    val difference = checkForUnknownRoles(si)

    difference.size!= 0 match
      case true => Left(DomainError.UnknownRequirementRole(difference.head))

      case false => 
        val s: Set[Shift] = si.schedulingPeriods.foldLeft[Set[Shift]](Set()){
          (acc, sp) =>
            createShifts(acc, si.constraints, sp.id, si.nurses, sp.nurseRequirement) match
              case Left(value) => acc
              case Right(value) => acc ++ Set(value)
        }

        if(s.size<si.schedulingPeriods.size)
          Left(DomainError.NotEnoughNurses)
        else

          val sets =
            for{
              d1 <- DayNumber.from(1)
              sd1 <- ShiftDay.from(d1, s)
              d2 <- DayNumber.from(2)
              sd2 <- ShiftDay.from(d2, s)
              d3 <- DayNumber.from(3)
              sd3 <- ShiftDay.from(d3, s)
              d4 <- DayNumber.from(4)
              sd4 <- ShiftDay.from(d4, s)
              d5 <- DayNumber.from(5)
              sd5 <- ShiftDay.from(d5, s)
              d6 <- DayNumber.from(6)
              sd6 <- ShiftDay.from(d6, s)
              d7 <- DayNumber.from(7)
              sd7 <- ShiftDay.from(d7, s)
            } yield  ShiftSchedule(Set(sd1,sd2,sd3,sd4,sd5,sd6,sd7))
          
          sets




  def createShifts(shifts: Set[Shift], constraints: Constraint, id: StringId, nurses: Set[Nurse], nr: Set[NurseRequirement]): Result[Shift] =
    val sn: Set[ShiftNurse] = nr.foldLeft[Set[ShiftNurse]](Set()){
      (acc, req) =>
        val filteredNurses = nurses.filter(n => n.roles.exists(role => role.toString == req.role.toString)).toList.sortWith(_.name.toString < _.name.toString)
        val tmp = filteredNurses.foldLeft[Set[ShiftNurse]](Set()){
          (nurses, nur) =>
            val nameString = nur.name
            val a1 = maxShiftsPerDayValidation(constraints, shifts, nameString)
            val a2 = !nurseAlreadyInShiftValidation(acc, nameString)
            val a3 = !nurseRequirementMet(nurses, req.role, req.number)
            
            if(a1 && a2 && a3)
              nurses ++ Set(ShiftNurse(nameString, req.role))
            else
              nurses
        }
        acc ++ tmp
    }
    val sumNumber = nr.foldLeft[Int](0){(sum, rr) => sum + rr.number}
    Shift.from(id,sn, sumNumber)

