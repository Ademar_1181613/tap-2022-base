package domain

type Result[A] = Either[DomainError,A]

enum DomainError:
  //XML
  case IOFileProblem(error: String)
  case XMLError(error: String)
  //Nurse
  case NotEnoughNurses
  case EmptyName
  case EmptyIdError(error: String)
  case NoShiftsError(error: String)
  case NoNursesError(error: String)
  case MissingDaysError(error: String)
  case SeniorityValueOutOfRange(number: Int)
  case NoRolesError(error: String)
  case InvalidRole(error: String)
  //SchedulingPeriod
  case InvalidPeriod(error: String)
  case NoNurseRequirementsError(error: String)
  case InvalidNumber(error: String)
  //Constraint
  case InvalidMinRestDays(error: String)
  case InvalidMaxShiftsPerDay(error: String)
  //Day and Period
  case PreferenceValueOutOfRange(number: Int)
  case InvalidValue(error: String)
  case InvalidDayValue(error: String)
  //Scheduling
  case InsufficientServiceNecessities(error: String)

  case UnknownRequirementRole(role: String)

  case DuplicateNurses
  case InvalidDaysQuantity
  case UnknownRolesNotDetected

  