package domain.schedule

import scala.xml.Elem
import domain.Result
import domain.shift.ShiftSchedule
import domain.ScheduleInfo
import algorithm.SchedulingAlgorithmUtils01.getShiftSchedule

object ScheduleMS01 extends Schedule:

  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml elements
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
  def create(xml: Elem): Result[Elem] =
    for {
      si <- ScheduleInfo.from(xml)
      shiftSchedule <- getShiftSchedule(si)
    } yield ShiftSchedule.toXML(shiftSchedule)
