package domain.schedule

import algorithm.SchedulingAlgorithmUtils03.*

import scala.xml.Elem
import domain.{Result, ScheduleInfo}
import domain.shift.ShiftSchedule


object ScheduleMS03 extends Schedule:

  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml elements
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
  def create(xml: Elem): Result[Elem] =
    for {
      si <- ScheduleInfo.from(xml)
      // LOOK AHEAD VERSION
      // shiftSchedule <- getShiftScheduleV2(si, Set(), Set(), Set(), Set())
      
      // BACKTRACKING VERSION
      shiftSchedule <- getShiftSchedule(si)
    } yield ShiftSchedule.toXML(shiftSchedule)