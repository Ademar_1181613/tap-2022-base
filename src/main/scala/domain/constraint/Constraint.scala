package domain.constraint
import domain.{DomainError, Result}
import domain.Simple.*
import xml.XML.*

import scala.xml.*

case class Constraint(minRestDaysPerWeek: MinRestDaysPerWeek, maxShiftsPerDay: MaxShiftsPerDay)

object Constraint:

  def from(xml: Node): Result[Constraint] =
    for{
      minRestDaysPerWeek <- fromAttribute(xml, "minRestDaysPerWeek")
      mrdp <- MinRestDaysPerWeek.from(minRestDaysPerWeek.toInt)
      maxShiftsPerDay <- fromAttribute(xml, "maxShiftsPerDay")
      mspd <- MaxShiftsPerDay.from(maxShiftsPerDay.toInt)
    }yield Constraint(mrdp, mspd)


  def from(minRestDaysPerWeek: MinRestDaysPerWeek, maxShiftsPerDay: MaxShiftsPerDay): Constraint =
      Constraint(minRestDaysPerWeek, maxShiftsPerDay)



