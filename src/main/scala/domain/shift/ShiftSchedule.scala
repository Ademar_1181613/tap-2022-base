package domain.shift

import domain.{DomainError, Result}
import scala.xml.*
import domain.Simple.*

case class ShiftSchedule (shiftDays: Set[ShiftDay])

object ShiftSchedule {

  def from(shiftDays: Set[ShiftDay]): Result[ShiftSchedule] =
    if(shiftDays.size == 7)
      Right(ShiftSchedule(shiftDays))
    else
      Left(DomainError.MissingDaysError("Incorrect number of Days in Shift Schedule"))

  def toXML(shiftSchedule: ShiftSchedule): Elem =
    
    <shiftSchedule xmlns="http://www.dei.isep.ipp.pt/tap-2022" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2022 ../../schedule.xsd ">
      {shiftSchedule.shiftDays.toList.sortWith(_.id.toString < _.id.toString).map(s => ShiftDay.toXML(s))}
    </shiftSchedule>
}