package domain.shift

import domain.{DomainError, Result}
import domain.Simple.*
import scala.xml.*

case class ShiftNurse (name: NurseName, role: Role)

object ShiftNurse {

  def from(name: NurseName, role: Role): Result[ShiftNurse] =
      if(NurseName.toString(name).nonEmpty)
        Right(ShiftNurse(name, role))
      else
        Left(DomainError.EmptyName)

  def toXML(shiftNurse: ShiftNurse): Elem =
    <nurse name={NurseName.toString(shiftNurse.name)} role={Role.toString(shiftNurse.role)}/>
}