package domain.shift

import domain.{DomainError, Result}
import domain.Simple.*
import scala.xml.*

case class ShiftDay (id: DayNumber, shifts: Set[Shift])

object ShiftDay {

  def from(id: DayNumber, shifts: Set[Shift]): Result[ShiftDay] =
    // if(shifts.size > 0)
        Right(ShiftDay(id, shifts))
    // else
    //     Left(DomainError.NoShiftsError("No shifts present in ShiftDay"))


  def from(id: DayNumber, shifts: Set[Shift], requiredNumber: Int): Result[ShiftDay] =
    if(shifts.size == requiredNumber)
        Right(ShiftDay(id, shifts))
    else
        Left(DomainError.NotEnoughNurses)

  def toXML(shiftDay: ShiftDay): Elem =
    <day id={DayNumber.toString(shiftDay.id)}>
      { shiftDay.shifts.map( s => Shift.toXML(s)) }
    </day>
}