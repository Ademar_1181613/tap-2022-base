package domain.shift

import domain.{DomainError, Result}
import domain.Simple.*
import scala.xml.*

case class Shift (id: StringId, nurses: Set[ShiftNurse])

object Shift {

  def from(id: StringId, nurses: Set[ShiftNurse], requiredNumber: Int): Result[Shift] =
    if(nurses.size == requiredNumber)
        Right(Shift(id, nurses))
    else
        Left(DomainError.NotEnoughNurses)
        
  def toXML(shift: Shift): Elem =
    <shift id={StringId.toString(shift.id)}>
      { shift.nurses.toList.sortWith(_.name.toString < _.name.toString).map( n => ShiftNurse.toXML(n)) }
    </shift>
}