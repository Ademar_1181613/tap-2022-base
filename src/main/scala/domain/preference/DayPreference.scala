package domain.preference
import domain.{DomainError, Result}
import domain.Simple.{DayNumber, *}
import domain.preference.*
import xml.XML.*

import scala.xml.Node

case class DayPreference (Value: PreferenceValue, NurseId: StringId, DayValue: DayNumber) extends Preference

object DayPreference:

  def from(xml: Node): Result[DayPreference] =
    for{
      v <- fromAttribute(xml, "value")
      value <- PreferenceValue.from(v.toInt)
      n <- fromAttribute(xml, "nurse")
      nurseId <- StringId.from(n)
      d <- fromAttribute(xml, "day")
      dayValue <- DayNumber.from(d.toInt)
    }yield DayPreference(value, nurseId, dayValue)


  def from(prefValue: PreferenceValue, nurseId: StringId, dayValue: DayNumber): DayPreference =
      DayPreference(prefValue, nurseId, dayValue)


  def from(nurseId: StringId, dayValue: DayNumber): Result[DayPreference] =
      for{
        p<-PreferenceValue.from(0)
      }yield DayPreference(p, nurseId, dayValue)

