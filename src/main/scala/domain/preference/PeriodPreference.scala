package domain.preference
import domain.{DomainError, Result}
import domain.Simple.*
import domain.preference.*
import xml.XML.*

import scala.xml.Node


case class PeriodPreference (Value: PreferenceValue, PeriodId: StringId, NurseId: StringId) extends Preference

object PeriodPreference:

  def from(xml: Node): Result[PeriodPreference] =
    for{
      v <- fromAttribute(xml, "value")
      value <- PreferenceValue.from(v.toInt)
      p <- fromAttribute(xml, "period")
      periodId <- StringId.from(p)
      n <- fromAttribute(xml, "nurse")
      nurseId <- StringId.from(n)
    }yield PeriodPreference(value, periodId, nurseId)

  def from(prefValue: PreferenceValue, periodId: StringId, nurseId: StringId): PeriodPreference =
      PeriodPreference(prefValue, periodId, nurseId)


  def from(periodId: StringId, nurseId: StringId): Result[PeriodPreference] =
      for{
        p<-PreferenceValue.from(0)
      }yield PeriodPreference(p, periodId, nurseId)
