package domain

import domain.{DomainError, Result}
import domain.Simple.*
import domain.nurse.*
import domain.constraint.*
import domain.schedulingperiod.*
import domain.preference.*
import scala.xml.*
import xml.XML.*

case class ScheduleInfo (schedulingPeriods: Set[SchedulingPeriod],
                                 nurses: Set[Nurse],
                                 constraints: Constraint,
                                 dayPreferences: Set[DayPreference],
                                 periodPreferences: Set[PeriodPreference])

object ScheduleInfo:
  def from(xml: Elem): Result[ScheduleInfo] =
    for {
      nurses <- traverse((xml \\ "nurse"), Nurse.from)
      schedulingPeriod <- traverse((xml \\ "schedulingPeriod"), SchedulingPeriod.from)
      constraints <- traverse((xml \\ "constraints"), Constraint.from)
      dayPreferences <- traverse((xml \\ "dayPreference"), DayPreference.from)
      periodPreferences <- traverse((xml \\ "periodPreference"), PeriodPreference.from)
    } yield ScheduleInfo(schedulingPeriod.toSet,nurses.toSet,constraints.head,dayPreferences.toSet,periodPreferences.toSet)
