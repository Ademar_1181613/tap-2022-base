package domain

import scala.util.Try
import domain.Result
import scala.annotation.*

object Simple :
  opaque type StringId = String
  object StringId:
    def from(id: String): Result[StringId] =
      if(id.trim.nonEmpty)
        Right(id)
      else
        Left(DomainError.EmptyIdError("Invalid ID (empty)"))
    def toString(stringId: StringId): String =
      stringId


  opaque type Role = String
  object Role:
    def from(role: String): Result[Role] =
      if(role.trim.nonEmpty)
        Right(role)
      else
        Left(DomainError.InvalidRole("Invalid role (empty) for Nurse"))
    def toString(role: Role):String =
      role

  opaque type DayNumber = Int
  object DayNumber:
    def from(number: Int): Result[DayNumber] =
      if(number>0 && number<8)
          Right(number)
      else
          Left(DomainError.InvalidDayValue("Day Number is not valid (not in the 1-7 interval)"))
    def from2(number: Int): DayNumber =
      number
    def toString(day: DayNumber): String =
      day.toString
    def toInt(day: DayNumber): Int =
      day
    def decrementTwice(day: DayNumber): DayNumber =
      day - 2 
  extension (x: DayNumber)
    def + (y: Int): DayNumber =  x+y
    def - (y: Int): DayNumber =  x-y
    def same (y: Int): Boolean =  x==y
    def =< (y: DayNumber): Boolean =  x==y || x<y
    def >= (y: DayNumber): Boolean =  x>y || x==y
    def < (y: DayNumber): Boolean =  x<y
    def before (y: Int): Boolean =  x<y

  opaque type NurseName = String
  object NurseName:
    def from(name: String): Result[NurseName] =
      if(name.trim.nonEmpty)
        Right(name)
      else
        Left(DomainError.EmptyName)
    def toString(name: NurseName): String =
      name.toString
  
  opaque type NurseSeniority = Int
  object NurseSeniority:
    def from(seniority: Int): Result[NurseSeniority] =
      if(seniority>=1 && seniority<=5)
        Right(seniority)
      else
        Left(DomainError.SeniorityValueOutOfRange(seniority))

    def getValue(sen: NurseSeniority): Int =
      sen

  opaque type PreferenceValue = Int
  object PreferenceValue:
    def from(number: Int): Result[PreferenceValue]=
      if(number >= -2 && number <= 2)
        Right(number)
      else
        Left(DomainError.PreferenceValueOutOfRange(number))

    def getValue(preferenceValue: PreferenceValue): Int =
      preferenceValue

  opaque type MaxShiftsPerDay = Int
  object MaxShiftsPerDay:
    def from(number: Int): Result[MaxShiftsPerDay]=
      if(number > 0)
        Right(number)
      else
        Left(DomainError.InvalidMaxShiftsPerDay("Max shifts per day should be a positive number!"))

    def compareMaxShiftsPerDay(count: Int, maxShiftsPerDay: MaxShiftsPerDay): Boolean =
        if(count == maxShiftsPerDay) false else true

    def compareMaxShiftsPerDay2(count: Int, maxShiftsPerDay: MaxShiftsPerDay): Boolean =
        if(count > maxShiftsPerDay) true else false


  opaque type MinRestDaysPerWeek = Int
  object MinRestDaysPerWeek:
    def from(number: Int): Result[MinRestDaysPerWeek] =
      if(number >= 0 && number < 8)
        Right(number)
      else
        Left(DomainError.InvalidMinRestDays("Min Rest Days Per Week should be between 0 and 7"))
    def toInt(minRestDaysPerWeek: MinRestDaysPerWeek): Int =
      minRestDaysPerWeek

    def compareMinRestDaysPerWeek(count: Int, minRestDaysPerWeek: MinRestDaysPerWeek): Boolean =
      if(count == minRestDaysPerWeek) false else true


    
