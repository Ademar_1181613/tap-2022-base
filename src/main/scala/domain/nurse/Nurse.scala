package domain.nurse

import domain.{DomainError, Result}
import domain.Simple.*
import scala.xml.*
import xml.XML.*

case class Nurse (name: NurseName, seniority: NurseSeniority, roles: Set[Role])

object Nurse:

  def from(xml: Node): Result[Nurse] =
    for{
      name <- fromAttribute(xml, "name")
      nurseName <- NurseName.from(name)
      seniority <- fromAttribute(xml, "seniority")
      nurseSeniority <- NurseSeniority.from(seniority.toInt)
      roles <- traverse((xml \\ "nurseRole"), fromRole)
    } yield Nurse(nurseName, nurseSeniority, roles.toSet)

  def from(name: NurseName, seniority: NurseSeniority, roles: Set[Role]): Result[Nurse] =
    if (roles.size > 0)
      Right(Nurse(name, seniority, roles))
    else
      Left(DomainError.NoRolesError("The nurse must have at least 1 role"))


  def fromRole(xml: Node): Result[Role] =
    for {
      r <- fromAttribute(xml, "role")
      role <-Role.from(r)
    } yield role


      

