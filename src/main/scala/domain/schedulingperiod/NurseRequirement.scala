package domain.schedulingperiod

import domain.{DomainError, Result}
import domain.Simple.Role
import scala.xml.*
import xml.XML.*

case class NurseRequirement (role: Role, number: Int)

object NurseRequirement {

  def from(xml: Node): Result[NurseRequirement] =
    for {
      r <- fromAttribute(xml, "role")
      role <- Role.from(r)
      number <- fromAttribute(xml, "number")
    } yield NurseRequirement(role, number.toInt)

  def from(role: Role, number: Int): Result[NurseRequirement] =
      if(number > 0)
        Right(NurseRequirement(role, number))
      else
        Left(DomainError.InvalidNumber("NurseRequirement's number must be greater than 0"))
}