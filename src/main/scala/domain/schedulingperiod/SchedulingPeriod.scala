package domain.schedulingperiod

import domain.{DomainError, Result}
import domain.Simple.*
import scala.xml.*
import xml.XML.*

case class SchedulingPeriod (id: StringId, startTime: Time, endTime: Time, nurseRequirement: Set[NurseRequirement])

object SchedulingPeriod {

  def from(xml: Node): Result[SchedulingPeriod] =
    for {
      idString <- fromAttribute(xml, "id")
      id <- domain.Simple.StringId.from(idString)
      sTime <- fromAttribute(xml, "start")
      startTime <- Time.from(sTime)
      eTime <- fromAttribute(xml, "end")
      endTime <- Time.from(eTime)
      nurseRequirement <- traverse((xml \\ "nurseRequirement"), NurseRequirement.from)
    } yield SchedulingPeriod(id, startTime, endTime, nurseRequirement.toSet)

  
  def from(id: StringId, startTime: Time, endTime: Time, nurseRequirement: Set[NurseRequirement]): Result[SchedulingPeriod] =
    if(Time.compare(startTime, endTime) == -1)
      if(nurseRequirement.size > 0)
        Right(SchedulingPeriod(id, startTime, endTime, nurseRequirement))
      else
        Left(DomainError.NoNurseRequirementsError("SchedulingPeriod must have at least 1 NurseRequirement"))
    else 
      Left(DomainError.InvalidPeriod("Start time must be before End time"))
  
}