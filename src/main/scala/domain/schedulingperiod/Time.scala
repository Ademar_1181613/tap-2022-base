package domain.schedulingperiod

import domain.{DomainError, Result}
import domain.Simple.Role
import scala.xml.*

case class Time (hour: Int, minute: Int, second: Int)

object Time {

  def from(str: String): Result[Time] =
    val spl = str.split(":")
    val hour = spl(0).toInt
    val minute = spl(1).toInt
    val second = spl(2).toInt

    if(hour >= 0 && hour <= 23)
      if(minute >= 0 && minute <= 59)
        if(second >= 0 && second <= 59)
          Right(Time(hour, minute, second))
        else
          Left(DomainError.InvalidValue("Time: Second value is invalid (negative or more than 59)"))
      else
        Left(DomainError.InvalidValue("Time: Minute value is invalid (negative or more than 59)"))
    else
      Left(DomainError.InvalidValue("Time: Hour value is invalid (negative or more than 23)"))

  def from(hour: Int, minute: Int, second: Int): Result[Time] =
      if(hour >= 0 && hour <= 23)
        if(minute >= 0 && minute <= 59)
          if(second >= 0 && second <= 59)
            Right(Time(hour, minute, second))
          else
            Left(DomainError.InvalidValue("Time: Second value is invalid (negative or more than 59)"))
        else
          Left(DomainError.InvalidValue("Time: Minute value is invalid (negative or more than 59)"))
      else
        Left(DomainError.InvalidValue("Time: Hour value is invalid (negative or more than 23)"))


  def compare(t1: Time, t2: Time): Int =
    if(t2.hour == 0 && t2.minute == 0 && t2.second == 0)
      -1

    if(t1.hour == t2.hour)
      if(t1.minute == t2.minute)
        if(t1.second == t2.second)
          0
        else
          if(t1.second > t2.second)
            1
          else
            -1
      else
        if(t1.minute > t2.minute)
          1
        else
          -1
    else
      if(t1.hour > t2.hour)
        1
      else
        -1

          
}