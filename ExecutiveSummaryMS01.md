# Sumário Executivo Milestone 1

## Introdução

Este documento têm como objetivo descrever e justificar as decisões tomadas, analisar o resultado final e enúmerar possíveis melhorias relativas à primeira **Milestone** do projeto.

## Modelo de domínio

![Modelo de dominio](./src/diagrams/domain_model.png)

## Opaque Types

Neste projeto foram utilizados **opaque types** com o objetivo de não utilizar diretamente tipos primitivos e, desta forma, aplicar o conceito de **Single-responsability principle**, onde cada **opaque type** é responsável pela sua própria validação.

Para esse mesmo efeito, de forma a definirem-se os possíveis erros e podermos tratar os mesmos, foram criados os seguintes **Domain Errors**:

```

enum DomainError:
  //XML
  case IOFileProblem(error: String)
  case XMLError(error: String)
  //Nurse
  case NotEnoughNurses
  case EmptyName
  case EmptyIdError(error: String)
  case NoShiftsError(error: String)
  case NoNursesError(error: String)
  case MissingDaysError(error: String)
  case SeniorityValueOutOfRange(number: Int)
  case NoRolesError(error: String)
  case InvalidRole(error: String)
  //SchedulingPeriod
  case InvalidPeriod(error: String)
  case NoNurseRequirementsError(error: String)
  case InvalidNumber(error: String)
  //Constraint
  case InvalidMinRestDays(error: String)
  case InvalidMaxShiftsPerDay(error: String)
  //Day and Period
  case PreferenceValueOutOfRange(number: Int)
  case InvalidValue(error: String)
  case InvalidDayValue(error: String)
  //Scheduling
  case InsufficientServiceNecessities(error: String)
  case UnknownRequirementRole(role: String)

```

Desta forma, as **case classes** não têm a responsabilidade de validar conceitos de domínio.

Foram utilizados os seguintes **opaque types**:

```

object Simple :
  opaque type StringId = String
  object StringId:
    def from(id: String): Result[StringId] =
      if(id.trim.nonEmpty)
        Right(id)
      else
        Left(DomainError.EmptyIdError("Invalid ID (empty)"))
    def toString(stringId: StringId): String =
      stringId


  opaque type Role = String
  object Role:
    def from(role: String): Result[Role] =
      if(role.trim.nonEmpty)
        Right(role)
      else
        Left(DomainError.InvalidRole("Invalid role (empty) for Nurse"))
    def toString(role: Role):String =
      role

  opaque type DayNumber = Int
  object DayNumber:
    def from(number: Int): Result[DayNumber] =
      if(number>0 && number<8)
          Right(number)
      else
          Left(DomainError.InvalidDayValue("Day Number is not valid (not in the 1-7 interval)"))
    def toString(day: DayNumber): String =
      day.toString

  opaque type NurseName = String
  object NurseName:
    def from(name: String): Result[NurseName] =
      if(name.trim.nonEmpty)
        Right(name)
      else
        Left(DomainError.EmptyName)
    def toString(name: NurseName): String =
      name.toString
  
  opaque type NurseSeniority = Int
  object NurseSeniority:
    def from(seniority: Int): Result[NurseSeniority] =
      if(seniority>=1 && seniority<=5)
        Right(seniority)
      else
        Left(DomainError.SeniorityValueOutOfRange(seniority))

  opaque type PreferenceValue = Int
  object PreferenceValue:
    def from(number: Int): Result[PreferenceValue]=
      if(number >= -2 && number <= 2)
        Right(number)
      else
        Left(DomainError.PreferenceValueOutOfRange(number))

  opaque type MaxShiftsPerDay = Int
  object MaxShiftsPerDay:
    def from(number: Int): Result[MaxShiftsPerDay]=
      if(number > 0)
        Right(number)
      else
        Left(DomainError.InvalidMaxShiftsPerDay("Max shifts per day should be a positive number!"))

    def compareMaxShiftsPerDay(count: Int, maxShiftsPerDay: MaxShiftsPerDay): Boolean =
        if(count == maxShiftsPerDay) false else true


  opaque type MinRestDaysPerWeek = Int
  object MinRestDaysPerWeek:
    def from(number: Int): Result[MinRestDaysPerWeek] =
      if(number >= 0 && number < 8)
        Right(number)
      else
        Left(DomainError.InvalidMinRestDays("Min Rest Days Per Week should be between 0 and 7"))

```

Uma alternativa a esta abordagem, apesar de a nosso ver menos correcta, seria utilizar diretamente os tipos primitivos e validar os conceitos de domínio no construtor da classe em questão.

## Case Classes

De forma a representar os elementos apresentados no **Modelo de domínio** foram criadas **case classes**.
Cada uma das classes possui um método **from** responsável por transformar um **Xml Node** em um objeto do respétivo domínio.
Aseguir é apresentado o código referente a uma case classe, a Nurse, em que é demonstrado o método **from**, previamente referido.

```

case class Nurse private(name: NurseName, seniority: NurseSeniority, roles: Set[Role])

object Nurse:

  def from(xml: Node): Result[Nurse] =
    for{
      name <- fromAttribute(xml, "name")
      nurseName <- NurseName.from(name)
      seniority <- fromAttribute(xml, "seniority")
      nurseSeniority <- NurseSeniority.from(seniority.toInt)
      roles <- traverse((xml \\ "nurseRole"), fromRole)
    } yield Nurse(nurseName, nurseSeniority, roles.toSet)

  def from(name: NurseName, seniority: NurseSeniority, roles: Set[Role]): Result[Nurse] =
    if (roles.size > 0)
      Right(Nurse(name, seniority, roles))
    else
      Left(DomainError.NoRolesError("The nurse must have at least 1 role"))


  def fromRole(xml: Node): Result[Role] =
    for {
      r <- fromAttribute(xml, "role")
      role <-Role.from(r)
    } yield role

```

Com o objetivo de poder exportar para um ficheiro XML o resultado do **Scheduling Algorithm**,
várias classes implementam também um método **toXML**.

Essas classes são:
*   **ShiftNurse**
*   **Shift**
*   **ShiftDay**
*   **ShiftSchedule**

Este método têm como objetivo a conversão de objetos de domíno em elementos **XML**. Dessa forma podemos exportar para ficheiros **XML** o resultado final do **Scheduling Algorithm**.
De seguida, é apresentado um exemplo de um método **toXML** implementado. O método referente à conversão de um objeto **Shift** em um elemento **XML** que represente o mesmo.

```

  def toXML(shift: Shift): Elem =
    <shift id={StringId.toString(shift.id)}>
      { shift.nurses.map( n => ShiftNurse.toXML(n)) }
    </shift>

```


## Algoritmo

Para a criação de um ficheiro **output** com o **ShiftSchedule** elaborado, foi implementado um método **create** responsável por ler um ficheiro de **input** **XML**, percorrer toda a informação no mesmo e aplicar o algoritmo desenvolvido.
De seguida, é possivel verificar esse método **create** implementado.
```

  def create(xml: Elem): Result[Elem] =
    for {
      si <- ScheduleInfo.from(xml)
      shiftSchedule <- getShiftSchedule(si)
    } yield ShiftSchedule.toXML(shiftSchedule)

```

A lógica to algoritmo istá implementada na classe **SchedulingAlgorithmUtils01**.
Nesta classe temos o algoritmo principal presente no método **getShiftSchedule**, método este chamado no método **create**, previamente apresentado.

```

  def getShiftSchedule(si: ScheduleInfo): Result[ShiftSchedule] =
    val difference = checkForUnknownRoles(si)
    if(difference.size!=0)
      Left(DomainError.UnknownRequirementRole(difference.head))
    else
      val s: Set[Shift] = si.schedulingPeriods.foldLeft[Set[Shift]](Set()){
        (acc, sp) =>
          createShifts(acc, si.constraints, sp.id, si.nurses, sp.nurseRequirement) match
            case Left(value) => acc
            case Right(value) => acc ++ Set(value)
      }
      if(s.size<si.schedulingPeriods.size)
        Left(DomainError.NotEnoughNurses)
      else
        val sets =
          for{
            d1 <- DayNumber.from(1)
            sd1 <- ShiftDay.from(d1, s)
            d2 <- DayNumber.from(2)
            sd2 <- ShiftDay.from(d2, s)
            d3 <- DayNumber.from(3)
            sd3 <- ShiftDay.from(d3, s)
            d4 <- DayNumber.from(4)
            sd4 <- ShiftDay.from(d4, s)
            d5 <- DayNumber.from(5)
            sd5 <- ShiftDay.from(d5, s)
            d6 <- DayNumber.from(6)
            sd6 <- ShiftDay.from(d6, s)
            d7 <- DayNumber.from(7)
            sd7 <- ShiftDay.from(d7, s)
          } yield  ShiftSchedule(Set(sd1,sd2,sd3,sd4,sd5,sd6,sd7))
        sets

```


O Algoritmo principal consiste em:

1. Percorrer todos os **SchedulingPeriod's**
2. Obter o **Shift** correspondente a cada **SchedulingPeriod**, utilizando o método **createShifts**
3. Validar todas as constraints durante todo o processo utilizando a estrutura **if** e o método **checkForUnknownRoles**.
4. Criar um **ShifSchedule** para 7 dias, utilizando os Shifts obtidos com a etapa 2.

Por sua vez, o método **createShifts**, chamado na etapa dois, encontra-se no seguinte excerto de código:

```

  def createShifts(shifts: Set[Shift], constraints: Constraint, id: StringId, nurses: Set[Nurse], nr: Set[NurseRequirement]): Result[Shift] =
    val sn: Set[ShiftNurse] = nr.foldLeft[Set[ShiftNurse]](Set()){
      (acc, req) =>
        val filteredNurses = nurses.filter(n => n.roles.exists(role => role.toString == req.role.toString) )
        val tmp = filteredNurses.foldLeft[Set[ShiftNurse]](Set()){
          (nurses, nur) =>
            val nameString = nur.name.toString
            if(maxShiftsPerDayValidation(constraints, shifts, nameString) && !nurseAlreadyInShiftValidation(acc, nameString) && !nurseRequirementMet(nurses, req.role, req.number))
              val res =
                for{
                  nn <- ShiftNurse.from(nameString, req.role)
                } yield nn
              res match
                case Left(value) => nurses
                case Right(value) => nurses ++ Set(value)
            else
              nurses
        }
        acc ++ tmp
    }
    val sumNumber = nr.foldLeft[Int](0){(sum, rr) => sum + rr.number}
    Shift.from(id,sn, sumNumber)

```

O método **createShifts** consiste em:

1. Percorrer os **NurseRequirement's**
2. Para cada **NurseRequirement** obter as possíveis **Nurse's** a serem atribuitas ao **NurseRequirement**.
3. Percorrer as possiveis **Nurse's**
4. Para cada **Nurse**, se todas as validações tiverem sucesso, irá-se concatenar um Set[**NurseShift**] acumulativo com um novo Set, sendo que este possui uma nova **NurseShift**, sendo constituida pelo nome da **Nurse**, role do **NuresRequirement**
5. Obtendo um Set[NurseShift] que satisfaça as validações, irá se criar  um Shift como o conjunto de **NurseShift's** obtido na etapa anterior
## Testes

Foram desenvolvidas classes de teste de forma a validar todos os **opaque types**, **case classes** e uma classe extra para validar os métodos existentes na **SchedulingAlgorithmUtils**.

*   **Exemplo de testes aos opaque types**

```

class  SimpleTest extends AnyFunSuite:

  test("String id is empty"){
    val result = StringId.from("")
    assert(result.fold(error => error == DomainError.EmptyIdError("Invalid ID (empty)"), _ => false))
  }

  test("String id is blank"){
    val result = StringId.from("        ")
    assert(result.fold(error => error == DomainError.EmptyIdError("Invalid ID (empty)"), _ => false))
  }

  test("String id is created"){
    val result = StringId.from("identification")
    assert(result.fold(_ => false, _ => true))
  }

  test("Role is empty"){
    val result = Role.from("")
    assert(result.fold(error => error == DomainError.InvalidRole("Invalid role (empty) for Nurse"), _ => false))
  }

  test("Role is blank"){
    val result = Role.from("        ")
    assert(result.fold(error => error == DomainError.InvalidRole("Invalid role (empty) for Nurse"), _ => false))
  }

  test("Role is created"){
    val result = Role.from("role")
    assert(result.fold(_ => false, _ => true))
  }

  test("Day number is below 1"){
    val result = DayNumber.from(0)
    assert(result.fold(error => error == DomainError.InvalidDayValue("Day Number is not valid (not in the 1-7 interval)"), _ => false))
  }

  test("Day number is above 7"){
    val result = DayNumber.from(8)
    assert(result.fold(error => error == DomainError.InvalidDayValue("Day Number is not valid (not in the 1-7 interval)"), _ => false))
  }

  test("Day number is created"){
    val result = DayNumber.from(4)
    assert(result.fold(_ => false, _ => true))
  }

  test("NurseName is empty"){
    val result = NurseName.from("")
    assert(result.fold(error => error == DomainError.EmptyName, _ => false))
  }

  test("NurseName is blank"){
    val result = NurseName.from("        ")
    assert(result.fold(error => error == DomainError.EmptyName, _ => false))
  }

  test("NurseName is created"){
    val result = NurseName.from("Ana")
    assert(result.fold(_ => false, _ => true))
  }

  test("NurseSeniority is below 1"){
    val result = NurseSeniority.from(0)
    assert(result.fold(error => error == DomainError.SeniorityValueOutOfRange(0), _ => false))
  }

  test("NurseSeniority is above 5"){
    val result = NurseSeniority.from(6)
    assert(result.fold(error => error == DomainError.SeniorityValueOutOfRange(6), _ => false))
  }

  test("NurseSeniority is created"){
    val result = NurseSeniority.from(3)
    assert(result.fold(_ => false, _ => true))
  }

  test("PreferenceValue is created"){
    val result = PreferenceValue.from(2)
    assert(result.fold(_ => false, _ => true))
  }

  test("PreferenceValue is above 2"){
    val result = PreferenceValue.from(3)
    assert(result.fold(error => error == DomainError.PreferenceValueOutOfRange(3), _ => false))
  }

  test("PreferenceValue is below -2"){
    val result = PreferenceValue.from(-3)
    assert(result.fold(error => error == DomainError.PreferenceValueOutOfRange(-3), _ => false))
  }

  test("MaxShiftsPerDay is created"){
    val result = MaxShiftsPerDay.from(1)
    assert(result.fold(_ => false, _ => true))
  }

  test("MaxShiftsPerDay is below 0"){
    val result = MaxShiftsPerDay.from(-1)
    assert(result.fold(error => error == DomainError.InvalidMaxShiftsPerDay("Max shifts per day should be a positive number!"), _ => false))
  }

  test("MinRestDaysPerWeek is below 0"){
    val result = MinRestDaysPerWeek.from(-1)
    assert(result.fold(error => error == DomainError.InvalidMinRestDays("Min Rest Days Per Week should be between 0 and 7"), _ => false))
  }

  test("MinRestDaysPerWeek is above 7"){
    val result = MinRestDaysPerWeek.from(8)
    assert(result.fold(error => error == DomainError.InvalidMinRestDays("Min Rest Days Per Week should be between 0 and 7"), _ => false))
  }

  test("MinRestDaysPerWeek is created"){
    val result = MinRestDaysPerWeek.from(1)
    assert(result.fold(_ => false, _ => true))
  }

```

*   **Exemplo de testes às case classes**

```

class NurseTest extends AnyFunSuite:
  
  test("Nurse Seniority without roles"){
    val result = 
      for{
        nurseName <- NurseName.from("Ana")
        nurseSeniority <- NurseSeniority.from(3)
        roleA <- Role.from("A")
        roleB <- Role.from("B")
      } yield Nurse.from(nurseName, nurseSeniority,Set[Role]())
    assert(result.fold(error => error == DomainError.NoRolesError("The nurse must have at least 1 role"), _ => true))
  }

  test("Nurse is created"){
    val result = 
      for{
        nurseName <- NurseName.from("Ana")
        nurseSeniority <- NurseSeniority.from(3)
        roleA <- Role.from("A")
        roleB <- Role.from("B")
      } yield Nurse.from(nurseName, nurseSeniority,Set(roleA, roleB))
    assert(result.fold(_ => false, _ => true))
  }


```

*   **Exemplo de testes à classe SchedulingAlgorithmUtils**

```

class SchedulingAlgorithmUtils01Test extends AnyFunSuite:

  test("Max Shifts Per day reached"){
    val res =
      for {
        role <- Role.from("role")
        id1 <- StringId.from("morning")
        id2 <- StringId.from("afternoon")
        id3 <- StringId.from("evening")
        m1 <- MinRestDaysPerWeek.from(2)
        m2 <- MaxShiftsPerDay.from(3)
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
        shift1 <- Shift.from(id1, Set(nurse1, nurse2), 2)
        shift2 <- Shift.from(id2, Set(nurse1, nurse2), 2)
        shift3 <- Shift.from(id3, Set(nurse1, nurse2), 2)
      }
      yield SchedulingAlgorithmUtils01.maxShiftsPerDayValidation(Constraint.from(m1,m2),  Set(shift1,shift2,shift3), "Maria")
    assert(res.fold(_=>false, _=> res) == Right(false))
  }

  test("Max Shifts Per day not reached"){
    val res = 
      for {
        role <- Role.from("role")
        id1 <- StringId.from("morning")
        id2 <- StringId.from("afternoon")
        id3 <- StringId.from("evening")
        m1 <- MinRestDaysPerWeek.from(2)
        m2 <- MaxShiftsPerDay.from(3)
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
        nurse3 <- ShiftNurse.from("Joana", role)
        shift1 <- Shift.from(id1, Set(nurse1, nurse2), 2)
        shift2 <- Shift.from(id2, Set(nurse1, nurse2), 2)
        shift3 <- Shift.from(id3, Set(nurse1, nurse3), 2)
      }
      yield SchedulingAlgorithmUtils01.maxShiftsPerDayValidation(Constraint(m1,m2),  Set(shift1,shift2,shift3), "Maria")
    assert(res.fold(_=>false, _=> res) == Right(true))
  }

  test("Shift: nurse not yet in it"){
    val res = 
      for {        
        role <- Role.from("role")
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
      }
      yield SchedulingAlgorithmUtils01.nurseAlreadyInShiftValidation(Set(nurse1, nurse2), "Joana")
    assert(res.fold(_=>false, _=> res) == Right(false))
  }
  
  test("Shift: nurse is already in it"){
    val res = 
      for {
        role <- Role.from("role")
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
      }
      yield SchedulingAlgorithmUtils01.nurseAlreadyInShiftValidation(Set(nurse1, nurse2), "Ana")
    assert(res.fold(_=>false, _=> res) == Right(true))
  }


  test("Shift number of nurses is not met"){
    val res = 
      for {
        role <- Role.from("role")
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
      }
      yield SchedulingAlgorithmUtils01.nurseRequirementMet(Set(nurse1, nurse2), role, 4)
    assert(res.fold(_=>false, _=> res) == Right(false))
  }
  
  test("Shift number of nurses is met"){
    val res = 
      for {
        role <- Role.from("role")
        nurse1 <- ShiftNurse.from("Ana", role)
        nurse2 <- ShiftNurse.from("Maria", role)
      }
      yield SchedulingAlgorithmUtils01.nurseRequirementMet(Set(nurse1, nurse2), role, 2)
    assert(res.fold(_=>false, _=> res) == Right(true))
  }
  


```

## Testes ao Algoritmo

Para os testes do algoritmo foram utilizados os testes fornecidos no projeto base.



## Conclusões

Analisando o resultado obtido, é possível concluir que os objetivos propostos ao desenvolvimento da primeira **Milestone** foram atingidos com sucesso.
Foram implementadas todas as funcionalidades que foram requesitadas para o algoritmo, teve-se em consideração todas as boas práticas de desenvolvimento, adotou-se sempre a imutabilidade, obteve-se um elevado coverage de testes unitários e executaram-se, com sucesso, todos os testes de validação do algoritmo desenvolvido.