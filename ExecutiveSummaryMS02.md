# Sumário Executivo Milestone 2

## Introdução

Este documento têm como objetivo descrever e justificar as decisões tomadas, analisar o resultado final e enúmerar possíveis melhorias relativas à segunda **Milestone** do projeto.

## Geradores

Nesta **Milestone** foram utilizados geradores de forma a criar entidades do dominio com valores aleatórios.
Foram criados geradores ao nível dos simple types criados na primeira **Milestone**.

```
object SimpleGenerator extends Properties("Simple"):

  def genStringId: Gen[StringId]=
    for{
      n <- Gen.chooseNum(2,10)
      cl <- Gen.listOfN(n, Gen.alphaChar)
      id <- StringId.from(cl.mkString).fold(_ => Gen.fail, id => Gen.const(id))
    }yield id

  def genRole: Gen[Role]=
    for{
      cl <- Gen.oneOf("A", "B", "C")
      role <- Role.from(cl).fold(_ => Gen.fail, role => Gen.const(role))
    }yield role

  def genBadRole: Gen[Role]=
    for{
      cl <- Gen.oneOf("D", "E", "F")
      role <- Role.from(cl).fold(_ => Gen.fail, role => Gen.const(role))
    }yield role

  def genDayNumber: Gen[DayNumber]=
    for{
      value <- Gen.chooseNum(1,7)
      dayNumber <- DayNumber.from(value).fold(_ => Gen.fail, number => Gen.const(number))
    }yield dayNumber

  def genNurseName: Gen[NurseName]=
    for{
      n <- Gen.chooseNum(2,10)
      cl <- Gen.listOfN(n, Gen.alphaChar)
      nurseName <- NurseName.from(cl.mkString).fold(_ => Gen.fail, name => Gen.const(name))
    }yield nurseName

  def genNurseSeniority: Gen[NurseSeniority]=
    for{
      value <- Gen.chooseNum(1,5)
      nurseSeniority <- NurseSeniority.from(value).fold(_ => Gen.fail, seniority => Gen.const(seniority))
    }yield nurseSeniority

  def genPreferenceValue: Gen[PreferenceValue]=
    for{
      value <- Gen.chooseNum(-2,2)
      preferenceValue <- PreferenceValue.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    }yield preferenceValue

  def genMaxShiftsPerDay: Gen[MaxShiftsPerDay]=
    for{
      value <- Gen.chooseNum(2,6)
      maxShiftsPerDay <- MaxShiftsPerDay.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    }yield maxShiftsPerDay

  def genMinRestDaysPerWeek: Gen[MinRestDaysPerWeek]=
    for{
      value <- Gen.chooseNum(3,7)
      minRestDaysPerWeek <- MinRestDaysPerWeek.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    }yield minRestDaysPerWeek
```

E utilizando os geradores referente aos simple types, criaram-se os geradores para todas as diferentes entidades do dominio. De seguida podemos visualizar os geradores criados relativamente a um **NurseRequirement**:

```
object NurseRequirementGenerator extends Properties("NurseRequirements"):
  def genNurseRequirement(ln: List[Nurse]) : Gen[NurseRequirement] =
    val roles = ln.flatMap(nurse => nurse.roles)
    for{
      role <- Gen.oneOf(roles)
      num = ln.filter(n => n.roles.exists(rol => rol.toString == role.toString))
      number <- Gen.chooseNum(1,num.size)
    } yield NurseRequirement(role, number)
  
  def genListOfNurseRequirements(ln: List[Nurse]) : Gen[List[NurseRequirement]] =
    for {
      n <- Gen.chooseNum(1,3)
      lp = (1 to n).map(i => genNurseRequirement(ln)).toList
      list <- Gen.sequence[List[NurseRequirement], NurseRequirement](lp)
    } yield verifyRequirements(list, ln)

  def genBadNurseRequirement : Gen[NurseRequirement] =
    for{
      role <- genBadRole
      number <- Gen.chooseNum(1,3)
    } yield NurseRequirement(role, number)

  def genBadListOfNurseRequirements : Gen[List[NurseRequirement]] =
    for {
      n <- Gen.chooseNum(1,3)
      lp = (1 to n).map(i => genBadNurseRequirement).toList
      list <- Gen.sequence[List[NurseRequirement], NurseRequirement](lp)
    } yield list

  def verifyRequirements(lst: List[NurseRequirement], ln: List[Nurse]) : List[NurseRequirement] =
    val lnn = ln.sortWith(_.name.toString < _.name.toString).toSet
    val newObject = (Set[NurseRequirement](), lnn)
    val res = lst.sortWith(_.role.toString < _.role.toString).foldLeft(newObject){
      (acc, req) =>  
        val act = acc._2.filter(n => n.roles.exists(role => role.toString == req.role.toString)).toList.sortWith(_.name.toString < _.name.toString)
        val act2 = acc._1.exists(r => r.role.toString == req.role.toString)
        if(act.size >= req.number && !act2)
          val a1 = acc._1 ++ Set(req) 
          val a2 = acc._2 -- act.take(req.number)  
          (a1,a2)
          
        else
          acc
    }
    
    res._1.toList
```

É de realçar que foram desenvolvidos geradores tanto para casos de sucesso como para casos de insucesso.

## Property tests

Sendo que os geradores de entidades de Input já estão criados, resta agora testar o Output gerado atraves de property base tests.
Estes testes irão ser executados inúmeras vezes, originando então diversas combinações diferentes de input.

De seguida podemos visualizar dois exemplos de property base tests:

```
property("Nurse cannot be used in the same shift with different roles") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        r <- checkIfNurseIsNotRepeated(shiftSchedule)
      } yield r
    result.fold(_ => false, _ => true)
  )
  
  def checkIfNurseIsNotRepeated(shiftSchedule: ShiftSchedule) : Result[Boolean] =
    shiftSchedule.shiftDays.foldLeft[Result[Boolean]](Right(true))(
      (result1, days) =>
        days.shifts.foldLeft[Result[Boolean]](Right(true))(
          (result2, shifts) => 
            if (shifts.nurses.map(n => n.name).toSet.size != shifts.nurses.size) Left(DuplicateNurses) else result2
      )
    )

  property("The shift schedule must schedule all the requirements for each day") = forAll(genScheduleInfo) (info =>
    val result =
      for {
        shiftSchedule <- getShiftSchedule(info)
        r <- checkIfRequirementsAreMet(shiftSchedule, info)
      } yield r
    result.fold(_ => false, _ => true)
  )
  
  def checkIfRequirementsAreMet(shiftSchedule: ShiftSchedule, info: ScheduleInfo) : Result[Boolean] =
    shiftSchedule.shiftDays.foldLeft[Result[Boolean]](Right(true))(
      (result1, days) =>
        days.shifts.foldLeft[Result[Boolean]](Right(true))(
          (result2, shifts) => 
            val requirements = info.schedulingPeriods.filter(p => p.id.toString == shifts.id.toString).head.nurseRequirement
            requirements.foldLeft[Result[Boolean]](Right(true))(
              (result3, req) => 
                if(shifts.nurses.filter(n => n.role.toString == req.role.toString).size != req.number) Left(DuplicateNurses) else result2
            )
        )
    )

```
## Melhorias realizadas sobre a primeira Milestone

Nesta segunda **Milestone** procedeu-se à melhoria de dois tópicos da primeira iteração. 
A primeira melhoria foi em relação à utilização da estrutura "if else" no algoritmo. De forma a corrigir a mesma, passou-se a utilizar a estrutura "match case".

Por sua vez, a segunda melhoria consistiu em implementar-se testes de integração para além dos fornecidos. Dessa forma, implementaram-se 10 testes de integração que abrangem cenários de sucesso assim como de insucesso.


## Conclusões

Analisando o resultado obtido, é possível concluir que os objetivos propostos ao desenvolvimento da segunda **Milestone** foram atingidos com sucesso.
Foram criados geradores para todas as entidades do dominio assim como implementadas todas as propriedades requeridas.